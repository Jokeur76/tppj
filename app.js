var express = require('express');
var session = require('express-session');
var exphbs = require('express3-handlebars');
var bodyParser = require('body-parser');
var path = require('path');
var nodemailer = require('nodemailer');

var packagejson = require('./package.json');
fs = require('fs');
crypto = require('crypto');
global.__started = false;
if(!global.__base)
    global.__base = __dirname + '/';
if(!global.__env)
    global.__env = "run";

//own modules
myMongo = require(global.__base+'modules/myMongo.js');
serverTools = require(global.__base+'modules/serverTools.js');
var getPages = require(global.__base+'modules/getPages.js');
var getRequests = require(global.__base+'modules/getRequests.js');
var postRequests = require(global.__base+'modules/postRequests.js');
var helmet = require('helmet');
security = require(global.__base+'modules/security.js');
//Configuration serveur
app = express();
app.use(session({
    secret: '0987654321AZERTYUIOPMLKJHGFDSQWXCVBN?./§',
    resave: false,
    saveUninitialized: true,
    httpOnly: false
}));
app.use(express.static('assets'))
    // For Content-Type application/json
    .use(bodyParser.json())
    // For x-www-form-urlencoded
    .use(bodyParser.urlencoded({ extended: true }))
    .set('views', global.__base+'views/pages')
    .engine('handlebars', exphbs({defaultLayout: 'main'}))
    .set('view engine', 'handlebars')
    .use(helmet());
;
exphbs.ExpressHandlebars.prototype.layoutsDir = 'views';
//import conf
var config = JSON.parse(fs.readFileSync(global.__base+'conf/env.json', 'utf8'));
//import i18n
var contFr = JSON.parse(fs.readFileSync(global.__base+'resources/i18n/fr.json', 'utf8'));
//import conversion datas
var conversionObj = JSON.parse(fs.readFileSync(global.__base+'resources/enums/conversions.json', 'utf8'));

Object.defineProperty(Array.prototype, 'contains', {
    enumerable: false,
    value: function(elem) {
        return this.indexOf(elem) >= 0;
    }
});

Object.defineProperty(Object.prototype, 'cloneIt', {
    enumerable: false,
    value: function() {
        return JSON.parse(JSON.stringify(this));
    }
});

Object.defineProperty(Array.prototype, 'getPortionSum', {
    enumerable: false,
    value: function() {
        return this.reduce(function(sum, value) {
            if(value === undefined) {
                return 0;
            }
            if(value.portion_nbr === undefined) {
                return 0;
            }
            return sum + value.portion_nbr;
        }, 0)
    }
});

//app vars
var port = config.port;
var appUrl = config.url;
var toyostContent = {};
var sess = {};
var isCo;
var content;
var cont = contFr;

//VARIABLES GLOBALES
myMongo.createStartList('app.js').then(function (resolve) {
    global.__started = true;
});
//run routine to ensure of DB structure
myMongo.updateDB();

//mail sending
// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'jokeur.mailer@gmail.com',
        pass: "d0n't_n33d_2_r3m3mBEuR"
    }
});

exports.getAppUrl = function() {
    return appUrl;
};

exports.sendMail = function(options) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: '"TPPJ" <info@tppj.com>', // sender address
        to: options.to, //'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
        subject: options.subject, //'Hello ✔', // Subject line
        text: options.text, //'Hello world ?', // plain text body
        html: options.html //'<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

};

//Routage
app.use(function(req, res, next){
    isCo = serverTools.isHexaValid(req.session.userId);
    content = {cont: cont, toyostMsg: toyostContent.msg, toyostTimeout: toyostContent.timeout, isCo: isCo.toString()};
    sess = req.session;
    next();
});

exports.resetToyostContent = function() {
    toyostContent = {};
};
exports.setToyostContent = function(p_toyostContent) {
    toyostContent = p_toyostContent;
};
exports.setRedirectTo = function(redirectTo) {
    sess.redirectTo = redirectTo;
};
exports.getVersion = function() {
    return packagejson.version;
};
exports.getIsCo = function() {
    return isCo;
};
exports.setIsCo = function(p_isCo) {
    isCo = p_isCo;
};
exports.getContent = function() {
    return content;
};
exports.setContent = function(_content) {
    content = _content;
};
exports.getTrad = function() {
    return cont;
};
exports.getConversionObj = function() {
    return conversionObj;
};
exports.setContent = function(p_content) {
    content = p_content;
};
exports.getSess = function() {
    return sess;
};
exports.setSessParam = function(key, value) {
    sess[key] = value;
};
exports.saveSess = function() {
    sess.save();
};
exports.sessDestroy = function(req, res) {
    sess.destroy();
    res.redirect("/")
};

//Démarrage serveur
if(global.__env === "run")
    app.listen(port);

app.get('/', getPages.home)
    .get('/register', getPages.register)
    .get('/connect', getPages.connect)
    .get('/createmenu', getPages.createMenu)
    .get('/newcourse', getPages.newCourse)
    .get('/reset', getPages.reset)
    .get('/resetrequestsent', getPages.resetrequestsent)
    .get('/pwdreset', getPages.pwdreset)
    .get('/admin', getPages.admin)
    .get('/logout', exports.sessDestroy)
    //API GET
    .get('/get_ing', getRequests.ing)
    .get('/get_unit', getRequests.unit)
    .get('/get_cat', getRequests.cat)
    .get('/totalcourse', getRequests.totalCourses)
    .get('/isAdmin', getRequests.isAdmin)
    //post
    .post('/register', postRequests.register)
    .post('/connect', postRequests.connect)
    .post('/addcourse', postRequests.addCourse)
    .post('/addingredients', postRequests.addIngredients)
    .post('/menu', postRequests.menu)
    .post('/resetrequestsent', postRequests.resetrequestsent)
    .post('/resetpwd', postRequests.resetpwd)
    //redirection des pages non gérées
    .use(function(req, res){
        res.redirect('/');
    });
