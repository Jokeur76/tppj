var currentStep = "courseName";
var startStep = "courseName";
var asking = false;
var helpActivate = true;

$(function() {
    if(window.location.pathname === "/newcourse") {
        displayToyost();
        addIngredientsLine();
        $('#createCourse').click(checkMenuForm);
        $('.ingName').on('change', function() {
            autoSelectUnit(this)
        });
        $('#type_choice').select2({width: '27%'})
        if (localStorage.getItem("helpActivate") !== null) {
            helpActivate = localStorage.getItem("helpActivate") === "true";
            //if help activated checkbox unchecked
            $('#helpDeactivate').prop("checked", !helpActivate);
        }

        //add listener on the inputs to manage ing saving
        //if title input finish press enter
        addListeners();

        $(document).on('keydown', function (event) {
            //if press enter
            if (event.keyCode === 13) {
                //avoid trigger submit form when press enter by defaut behavior of bootstrap
                event.preventDefault();
                switch (currentStep) {
                    case "courseName":
                        //if title is not write : inform user that it is required
                        //else focus on first diet type
                        if ($('#course_name').val() === "") {
                            $('#course_name').focus();
                            toyostObj.msg = course_required_title;
                            displayToyost();
                        } else {
                            $('#type_choice').select2('open');
                        }
                        break;
                    case "typeChoice":
                        if ($('#type_choice').val() === null) {
                            $('#type_choice').select2('open');
                            toyostObj.msg = service_required;
                            displayToyost();
                        } else {
                            $('.diet_type:first').focus();
                        }
                        break;
                    case "dietChoice":
                        //check if a diet is choose
                        var oneDietChoosed = false;
                        $('.diet_type').each(function () {
                            if ($(this).is(':checked')) {
                                oneDietChoosed = true;
                            }
                        });

                        //if a diet is choosed : focus on ingredient name
                        //else inform user that it is required
                        if (oneDietChoosed) {
                            $('.ingSelect:last .ingName').select2('open');
                        } else {
                            toyostObj.msg = course_required_diet;
                            displayToyost();
                        }
                        break;
                    case "ingName":
                        //focus on last quantity input
                        $('.ingSelect:last .ingQtt').focus();
                        break;
                    case "ingQtt":
                        //if a qtt is written : focus on ingredient unit
                        //else inform user that it is required
                        if ($('.ingSelect:last .ingQtt').val() === "") {
                            $('.ingSelect:last .ingQtt').focus();
                            toyostObj.msg = course_required_qtt;
                            displayToyost();
                        } else {
                            $('.ingSelect:last .ingUnit').select2('open');
                        }
                        break;
                    case "ingUnit":
                        //after choosing/validating unit ask user if want add ingredient or go next step
                        $('#askForNewIng').removeClass('hide');
                        $('#askForNewIng .btn:first').focus();
                        break;
                    case "askIng":
                        if (!asking) {
                            resolveAsk(0);
                        }
                        break;
                    case "paramPvt":
                        $('#portion_nbr').focus();
                        break;
                    case "portionNbr":
                        $('#course_recipe_link').focus();
                        break;
                    case "recipeLink":
                        checkCourseForm();
                        break;
                }
            }
            //if press tab
            if (event.keyCode === 9) {
                switch (currentStep) {
                    case "askIng":
                        event.preventDefault();
                        //prevent default behavior to avoid that private step is jumped
                        if (!asking) {
                            resolveAsk(1);
                        }
                        break;
                }
            }
            //if press escape
            if (event.keyCode === 27) {
                closeHelp();
            }
        })
    }
});

function addListeners() {
    $('.selectTwo').unbind('select2:opening');
    $('.selectTwo').on('select2:opening', function(event) {
        startNewStep($(this).attr('rel'));
    });

    $('.focusStep').unbind('focus');
    $('.focusStep').on('focus', function(event) {
        startNewStep($(this).attr('rel'));
    });
}

function startNewStep(step) {
    currentStep = step;
    closeHelp();
    displayHelp(step);
}

function displayHelp(id) {
    if(helpActivate) {
        $('#'+id).detach().appendTo('.panel-primary .panel-body.'+id).removeClass('forceHide');
    }
}

function closeHelp() {
    $('.helpers:not(.forceHide)').detach().appendTo('#content').addClass('forceHide');
}

function resolveAsk(index) {
    $('#askForNewIng').addClass('hide');
    switch(index) {
        case 0:
            //if user press enter  add an ingredient line and open the name part
            addIngredientsLine().then(function() {
                setTimeout(function() {
                    $('#units'+$('.ingSelect').length-1).select2('close');
                    $('#ingredients'+$('.ingSelect').length).select2('open');
                }, 200)
            }, function(err) {
                console.log('promise failed : ' + err);
            });
            break;
        case 1:
            //if user press tab focus on private checkbox
            $('#private').focus();
            break;
    }

}

function removeIng(id) {
    $('#'+id).remove();
}

function addIngredientsLine() {
    if(!asking) {
        asking = true;
        return new Promise(function (resolve, reject) {
            addIngTemplate();
            getIngForSelect().then(function () {
                getUnitForSelect().then(function () {
                    $('.ingName').trigger("change");
                    if (currentStep === startStep) {
                        $('#course_name').focus();
                    }
                    asking = false;
                    addListeners();
                    return resolve();
                });
            });
        })
    }
}

function autoSelectUnit(select) {
    var selectedOption = $('option[value="'+select.value+'"]');
    $('#'+select.id+'Div .ingUnit').val(selectedOption.attr('rel')).trigger("change");
}

function helpActivation(isChecked) {
    helpActivate = !isChecked;
    localStorage.setItem("helpActivate", !isChecked);
}