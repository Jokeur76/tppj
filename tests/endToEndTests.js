global.__base = __dirname.replace('tests', '');

var Browser = require('zombie'),
    browser = new Browser();
var chai = require('chai'),
    zombiedChai = require('zombied-chai'),
    expect = chai.expect,
    util = require('util');

var should = chai.should();
chai.use(zombiedChai);

//get config from env file
var fs = require("fs");
var config = JSON.parse(fs.readFileSync(global.__base+'conf/env.json', 'utf8'));
var contFr = JSON.parse(fs.readFileSync(global.__base+'resources/i18n/fr.json', 'utf8'));

var myMongo = require('../modules/myMongo.js');
//prepare test values
let ingSize = 509;
let catSize = 15;
let unitSize = 13;
var courses = [
    {
        title: "plat de test d'intégration",
        diet: "diet_type-omni",
        ings: [
            {
                name: "chocolat",
                qtt: 500,
                unit: "Gramme(s)"
            },
            {
                name: "miel",
                qtt: 30,
                unit: "Gramme(s)"
            }
        ],
        isPrivate: false,
        recipeLink: "http://deletemeiamatest.mika"
    }
];

myMongo.removeCourseByTitle(courses[0].title);

Browser.localhost(config.zombie.url, config.zombie.port);

describe('User arrive on landing page', function() {
    const browser = new Browser();

    before(function(done) {
        browser.visit('/', done);
    });

    it('should see the recipe number', function() {
        browser.should.have.element('#nbrRecipes');
        var nbrRecipes = browser.text("#nbrRecipes");
        nbrRecipes.should.not.be.equal("/o\\");
        nbrRecipes.should.not.be.equal("");
    });

    describe('User should be able to register', function() {

        before(function(done) {
            browser.click('#register a', done);
        });

        it('should be successful', function() {
            browser.assert.success();
        });

        it('should see register page', function() {
            browser.should.have.url().withPath('/register');
        });

        describe('submits register form', function() {
            var user;
            before(function(done) {
                myMongo.getUserTestIndex(function(index) {
                    user = 'user'+index+'@test.fr';
                    browser
                        .fill('register_login', user)
                        .fill('register_pwd', 'test')
                        .fill('register_pseudo', 'test')
                        .fill('register_diet', 'test')
                        .click('#register_btn', done);
                });
            });

            it('connexion should be successful', function() {
                browser.assert.success();
            });

            it('should see create menu page', function() {
                console.log("user : "+user);
                browser.should.have.url().withPath('/createmenu');
            });

            it('should see deco link', function() {
                browser.should.have.element('#logout');
            });

            it('should be connected', function() {
                expect(browser.window.isCo).to.be.equal(true);
            });

            describe('disconnect', function() {
                before(function(done) {
                    browser.click('#logout a', done);
                });

                it('connexion should be successful', function() {
                    browser.assert.success();
                });

                it('should see connection link', function() {
                    browser.should.have.element('#login');
                });

                it('should be not connected', function() {
                    expect(browser.window.isCo).to.be.equal(false);
                });

                it('should see home page', function() {
                    browser.should.have.url().withPath('/');
                });

                describe('User visits signup page', function() {
                    before(function(done) {
                        browser.click('#login a', done);
                    });

                    it('should be successful', function() {
                        browser.assert.success();
                    });

                    it('should see connect page', function() {
                        browser.should.have.url().withPath('/connect');
                    });

                    describe('submits connect form', function() {

                        before(function(done) {
                            browser
                                .fill('connect_login', 'user@test.fr')
                                .fill('connect_pwd', 'test')
                                .click('#connexion_btn', done);
                        });

                        it('connexion should be successful', function() {
                            browser.assert.success();
                        });

                        it('should see welcome page', function() {
                            browser.should.have.url().withPath('/createmenu');
                        });

                        it('should see deco link', function() {
                            browser.should.have.element('#logout');
                        });

                        describe("create a menu", function() {
                            var people_nbr = 2, duration = 7;
                            before(function(done) {

                                browser
                                    .choose('.diet_type')
                                    .click("#order_menu_btn", done);
                            });


                            it('create menu should be successful', function() {
                                browser.assert.success();
                            });

                            it('should have 14 portions cumulated in noon and evening meals', function() {
                                var noonMeals = browser.window.$(".noon.meal."+contFr.type_main_course+" .nbrPortion");
                                var sum = 0;
                                for(var noonMeal of noonMeals) {
                                    sum += parseInt(noonMeal.textContent);
                                }
                                assert.equal(sum, people_nbr*duration);

                                var eveningMeals = browser.window.$(".evening.meal."+contFr.type_main_course+" .nbrPortion");
                                var sum = 0;
                                for(var eveningMeal of eveningMeals) {
                                    sum += parseInt(eveningMeal.textContent);
                                }
                                assert.equal(sum, people_nbr*duration);
                            });

                        });

                        describe("add a meal", function() {
                            before(function(done) {
                                browser.click('#course a', done);
                            });

                            it('go to meal page should be successful', function() {
                                browser.assert.success();
                            });

                            it('should see add meal page', function() {
                                browser.should.have.url().withPath('/newcourse');
                            });

                            it('should have content in ing selects', function() {
                                var ingSelect = browser.window.$("#ingredients1");
                                expect(ingSelect.find('option').size()).to.be.equal(ingSize);
                                var unitSelect = browser.window.$("#units1");
                                expect(unitSelect.find('option').size()).to.be.equal(unitSize);
                            });

                            describe("create a new meal", function() {
                                var savedCourse = null;
                                before(function(done) {

                                    //fill title and set diet
                                    browser
                                        .fill('#course_name', courses[0].title)
                                        .choose("#"+courses[0].diet);
                                    //choose ingredient
                                    browser.window.$('#ingredients1').val(courses[0].ings[0].name);

                                    browser.window.$('#units1').val(courses[0].ings[0].unit);

                                    //fill qtt and add new ing
                                    browser
                                        .fill('#ingredients1Div .ingQtt', courses[0].ings[0].qtt);
                                    if(!courses[0].isPrivate) {
                                        browser.click('#private');
                                    }
                                    browser.fill("#course_recipe_link", courses[0].recipeLink)
                                    browser.click('#createCourse', done);
                                });

                                it('create meal should be successful', function() {
                                    browser.assert.success();
                                });

                                it('meal should have been saved', function(done) {
                                    myMongo.getCourseByTitle(courses[0].title, function(result) {
                                        savedCourse = result;
                                        should.exist(savedCourse);
                                        done();
                                    });
                                });

                                it('meal should contain corrects data', function() {
                                    console.log(util.inspect(savedCourse));
                                    should.exist(savedCourse)
                                    savedCourse.name.should.be.equal(courses[0].title);
                                    savedCourse.diet.should.be.equal(1);
                                    savedCourse.ingList[0].name.should.be.equal(courses[0].ings[0].name);
                                    savedCourse.ingList[0].qtt.should.be.equal(courses[0].ings[0].qtt);
                                    savedCourse.pv.should.be.equal(courses[0].isPrivate);
                                    savedCourse.recLink.should.be.equal(courses[0].recipeLink);

                                });
                            });
                        });
                    });
                });
            });
        });
    });
});