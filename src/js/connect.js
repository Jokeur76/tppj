$(function(){
    if(window.location.pathname === "/connect") {
        displayToyost();

        $('#connexion_btn').click(function() {
            $('#connectForm').submit();
        });
        $('#connect_pwd').on('keypress', function(event) {
            if(event.keyCode === 13)
            $('#connectForm').submit();
        })
    }
});

function showPwd() {
    var pwd = $('#connect_pwd');
    if(pwd.attr('type') == "password") {
        pwd.attr('type', 'text');
    } else {
        pwd.attr('type', 'password');
    }
}