$(function() {
    if(window.location.pathname === "/unauthorized") {
        //pas la peine de désactiver cette fonction vous etes déjà déconnecté
        setTimeout(function(){
            redirect("/");
        }, 8000);
    }
});