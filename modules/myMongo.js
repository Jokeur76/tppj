module.exports = {
    aggregate:aggregate,
    countCollection: countCollection,
    createStartList: createStartList,
    createUser: createUser,
    dropTestDB: dropTestDB,
    getBuyUnitFromName: getBuyUnitFromName,
    getCollection: getCollection,
    getCourseByTitle: getCourseByTitle,
    getUserByLogin: getUserByLogin,
    getUserTestIndex: getUserTestIndex,
    insertDocument: insertDocument,
    isHashValid: isHashValid,
    isLoginAvailable:isLoginAvailable,
    isUserAdmin: isUserAdmin,
    removeCourseByTitle: removeCourseByTitle,
    replacePwd: replacePwd,
    saveMenu: saveMenu,
    saveRequest: saveRequest,
    updateDB: updateDB,
    //accessors
    setUrl: setUrl,
    //unused for now
    deleteAutoDatas: deleteAutoDatas,
    deleteTestDatas: deleteTestDatas,
    removeCourseById: removeCourseById,
    getLastMenu: getLastMenu
};

mongoclient = require('mongodb').MongoClient;
ObjectId = require('mongodb').ObjectID;
assert = require('assert');
var fs = require('fs');
var dbName = JSON.parse(fs.readFileSync(global.__base+'conf/env.json', 'utf8')).dbName;

//mongo url
var url = 'mongodb://localhost:27017/'+dbName;

//VARIABLES GLOBALES
/** collections mongos */
COL_USER = "user";
COL_COURSES = "courses";
COL_INGREDIENTS = "ingredients";
COL_UNITS = "units";
COL_CATS = "cats";
COL_RESET = "reset";
COL_MENU = "menu";

//simple method => dont test
function aggregate(aggregate, callback) {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).aggregate(aggregate)
            .toArray(function(err, results){
                callback(err, results);
            });
    });
}

function countCollection(collec, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);

        db.collection(collec).count(callback);
    });

}

function createStartList() {
    var promiseArray = [];
    let ingCount, catCount, unitCount;
    var promises = new Promise(function(resolve, reject) {
        mongoclient.connect(url, function(err, db){
            assert.equal(null, err, "Impossible to connect to db");
            var promiseIng  = new Promise(function(resolve, reject) {
                db.collection(COL_INGREDIENTS).count(function (err, count) {
                    ingCount = count;
                    if(global.__env === "run") console.log(count);
                    var ingredients = JSON.parse(fs.readFileSync(global.__base+'resources/enums/ingredients.json', 'utf8'));
                    if (count < ingredients.length) {
                        db.collection(COL_INGREDIENTS).deleteMany({"test": true});
                        db.collection(COL_INGREDIENTS).deleteMany({"auto": true});
                        db.collection(COL_INGREDIENTS).insertMany(ingredients, function (err, result) {
                            assert.equal(err, null);
                            //console.log("Inserted a document into the ingredients collection.");
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            });
            promiseArray.push(promiseIng);
            var promiseUnits  = new Promise(function(resolve, reject) {
                db.collection(COL_UNITS).count(function(err, count){
                    unitCount = count;
                    if(global.__env === "run") console.log(count);
                    var units = JSON.parse(fs.readFileSync(global.__base+'resources/enums/units.json', 'utf8'));
                    if(count < units.length) {
                        db.collection(COL_UNITS).deleteMany({"test": true});
                        db.collection(COL_UNITS).deleteMany({"auto": true});
                        db.collection(COL_UNITS).insertMany(units, function (err, result) {
                            assert.equal(err, null);
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            });
            promiseArray.push(promiseUnits);
            var promiseCats  = new Promise(function(resolve, reject) {
                db.collection(COL_CATS).count(function(err, count){
                    catCount = count;
                    if(global.__env === "run") console.log(count);
                    var cats = JSON.parse(fs.readFileSync(global.__base+'resources/enums/cats.json', 'utf8'));
                    if(count < cats.length) {
                        db.collection(COL_CATS).deleteMany({"test": true});
                        db.collection(COL_CATS).deleteMany({"auto": true});
                        db.collection(COL_CATS).insertMany(cats, function (err, result) {
                            assert.equal(err, null);
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            });
            promiseArray.push(promiseCats);
            Promise.all(promiseArray).then(function(data) {
                if(global.__env === "run") {
                    console.log("###########################################");
                    console.log("############### APP STARTED ###############");
                    console.log("############### ingCount : " + ingCount + " ############");
                    console.log("############### unitCount : " + unitCount + " #############");
                    console.log("############### catCount : " + catCount + " #############");
                    console.log("###########################################");
                }
                resolve()
            })
        });
    });
    return promises;
}

function createUser(doc, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(COL_USER).insertOne(doc, function(err, result) {
            assert.equal(err, null);
            callback(result);
        });
    });
}

//test method : dont test
function dropTestDB() {
    return new Promise(function(resolve, reject) {
        mongoclient.connect(url, function(err, db){
            assert.equal(null, err, function() {
                reject("not connected")
            });
            assert.equal('mongodb://localhost:27017/tppj-test', url, function() {
                reject("url is not tppj-test")
            });

            db.dropDatabase();
            resolve("db dropped");
        });
    })
}

function getBuyUnitFromName(ingName, callback) {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_INGREDIENTS).find({name: ingName}, {_id: 0, buyUnit: 1})
            .toArray(function (err, result) {
                assert.equal(err, null);
                if(result.length > 0) {
                    callback(result[0].buyUnit)
                } else {
                    callback(null);
                }
            })
    });
}

function getCollection(collection) {
    return new Promise(function(resolve, reject) {
        mongoclient.connect(url, function(err, db){
            assert.equal(null, err);

            db.collection(collection).find().toArray(function(err, result) {
                assert.equal(err, null, () => { reject(err); });
                resolve(result);
            });
        });

    })
}

function getCourseByTitle(title, callback) {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).find({name: title})
            .toArray(function(err, results){
                assert.equal(null, err);
                callback(results.length === 0 ? null : results[0]);
            });
    });
}

function getUserByLogin(login, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(COL_USER).find({"login": login}).toArray(function(err, result) {
            assert.equal(err, null);
            var user = result[0];
            if(result.length > 1) {
                console.log("More than 1 user with this login");
            } else if(user == undefined) {
                user = null;
            }
            callback(user);
        });
    });
}

function getUserTestIndex(callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(COL_USER).count({login: {$regex: /user[0-9]*@test.fr/}}, function(err, result) {
            assert.equal(null, err);
            callback(result === 0 ? "" : result.toString());
        })
    })
}

function insertDocument(collec, doc, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(collec).insertOne(doc, function(err, result) {
            assert.equal(err, null);
            callback(result);
        });
    });
}

function isHashValid(hash, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(COL_RESET).find({hash: hash}).toArray(function (err, results) {
            assert.equal(null, err);
            var result = false
            if(results.length > 0) {
                if(new Date().getTime() - results[0].moment < 24*60*60*1000) {
                    result = true;
                }
            }
            callback(result);
        })
    })
}

function isLoginAvailable(login, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(COL_USER).find({"login": login}).toArray(function(err, result) {
            assert.equal(err, null);
            callback(result.length == 0);
        });
    });
}

function isUserAdmin(userId, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        var userObjectId =  new ObjectId();
        if(ObjectId.isValid(userId)) {
            userObjectId = ObjectId.createFromHexString(userId.toString());
        }
        db.collection(COL_USER).find({"_id": userObjectId}).toArray(function(err, result) {
            assert.equal(err, null);
            var user = result[0];
            if(result.length > 1) {
                console.log("More than 1 user with this login");
            }
            if(user != undefined) {
                callback(user.isAdmin);
            } else {
                callback(false);
            }
        });
    });
}

//method for tests => don't test
function removeCourseByTitle(title) {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).removeMany({name: title});
    });
}

function replacePwd(pwd, hash,  callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(COL_RESET).find({hash: hash}).toArray(function (err, results) {
            var pwdData = serverTools.saltHashPassword(pwd);
            db.collection(COL_USER).updateOne({login: results[0].emailToReset}, {$set: {pwd: pwdData.passwordHash, salt: pwdData.salt}}, function (err, results) {
                assert.equal(null, err);
                callback(results.result.nModified >= 1 && results.result.ok === 1);
            })
        })
    })
}

//simple save => dont test
function saveMenu(menu, callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(COL_MENU).insertOne(menu, function(err, results) {
            assert.equal(null, err);
            callback(err, results.result.n >= 1 && results.result.ok === 1);
        })
    })
}

//simple save => dont test
function saveRequest(emailToReset, callback) {
    //create unique code to identify request
    var requestId = serverTools.saltHashPassword(emailToReset+(new Date().getTime().toString())).passwordHash;
    var doc = {
        hash: requestId,
        emailToReset: emailToReset,
        moment: new Date().getTime()
    };
    mongoclient.connect(url, function(err, db){
        db.collection(COL_RESET).insertOne(doc, function(err, result) {
            assert.equal(null, err);
            callback(requestId, result)
        })
    });
}

function updateDB() {
    updateDB_add_course_type();
    updateDB_add_creation_date();
}

function deleteTestDatas(collec) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(collec).deleteMany({"test": true});
    });
}

function deleteAutoDatas(collec) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err, function(){
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
        });
        db.collection(collec).deleteMany({"auto": true});
    });
}

function removeCourseById(id) {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).removeOne({_id: id});
    });
}

function getLastMenu(callback) {
    mongoclient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(COL_MENU).find({})
            .sort({creation_date: -1})
            .limit(1).toArray(function(err, results) {
                assert.equal(null, err);
                if(results.length > 0) {
                    callback(results[0]);
                } else {
                    throw new Error("pas de menu");
                }
            })
    })
}

//accessors
function setUrl(p_url) {
    url = p_url;
}

//private functions

/**
 * search for courses without any type and add type "main course" by default
 * main courses: 4
 * starters: 2
 * desserts: 1
 */
function updateDB_add_course_type() {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).updateMany({type: {$exists: false}}, {$set: {type: 4}});
    });
}

function updateDB_add_creation_date() {
    mongoclient.connect(url, function(err, db){
        db.collection(COL_COURSES).find({creation_date: {$exists: false}})
            .toArray(function(err, results) {
                if(null === err && results.length > 0) {
                    var objId;
                    for(result of results) {
                        objId = result._id;
                        db.collection(COL_COURSES).updateOne({_id: objId}, {$set: {creation_date: objId.getTimestamp().getTime()}});
                    }
                }
            });
    });
}