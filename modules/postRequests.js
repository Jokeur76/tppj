let postRequests = {
    register: register,
    connect: connect,
    addCourse: addCourse,
    addIngredients: addIngredients,
    menu: menu,
    resetrequestsent: resetrequestsent,
    resetpwd: resetpwd,
    //for tests
    createMenu: createMenu,
    createIngList: createIngList,
    normalizeIng: normalizeIng,
    createIngHtmlList: createIngHtmlList,
    normalizeIng: normalizeIng,
    getCoursesFromDb: getCoursesFromDb
};

let app = require(global.__base+'app.js');
let util = require('util');

function register(req, res, next){
    let newUser = {};
    let redirect = "/";
    myMongo.isLoginAvailable(req.body.register_login, function(available) {
        if(available) {
            newUser.signUp = new Date().getTime();
            newUser.login = req.body.register_login;
            let pwdData = serverTools.saltHashPassword(req.body.register_pwd);
            newUser.pwd = pwdData.passwordHash;
            newUser.salt = pwdData.salt;
            newUser.pseudo = req.body.register_pseudo;
            newUser.diet = req.body.register_diet;
            myMongo.createUser(newUser, function(result) {
                app.setToyostContent({
                    msg: app.getTrad().toyost_signin_ok,
                    timeout: 3000
                });
                //connect user
                app.setSessParam("userId", result["ops"][0]["_id"]);
                app.saveSess();

                res.redirect("createmenu");
            });
        } else {
            app.setToyostContent({
                msg: app.getTrad().toyost_mail_exist,
                timeout: 5000
            });
            res.redirect("register");
        }
    })
      .catch(next);

}

function connect(req, res, next){
    assert.notEqual(null || undefined, req.body.connect_login, function () {
        app.setToyostContent({
            msg: app.getTrad().toyost_error,
            timeout: 5000
        });
        res.redirect("register");
    });

    //recuperer le user par son login + mdp crypté
    myMongo.getUserByLogin(req.body.connect_login, function(user){

        if(user === null) {
            //login unknow, inform user
            app.setToyostContent({
                msg: app.getTrad().toyost_mail_not_exist,
                timeout: 5000
            });
            res.redirect("connect");
        } else if("" === req.body.connect_pwd){
            //not pwd sent, inform user
            app.setToyostContent({
                msg: app.getTrad().toyost_required_login,
                timeout: 5000
            });
            res.redirect("connect");
        } else if(serverTools.checkPwd(req.body.connect_pwd, user.pwd, user.salt)){
            app.setSessParam("userId", user._id);
            app.setSessParam("isAdmin", user.isAdmin);
            app.saveSess();
            res.redirect('createmenu');
        } else {
            app.setToyostContent({
                msg: app.getTrad().toyost_error,
                timeout: 5000
            });
            res.redirect("connect");
        }
    })
      .catch(next)
}

function addCourse(req, res, next){
    if(app.getIsCo()) {
        let ingList = [];
        let ing = {};
        let pv = true;
        let bodyIng = req.body.ing;
        for(let i = 0; i < bodyIng.length; i++) {
            ing.name = bodyIng[i].name;
            ing.qtt = parseInt(bodyIng[i].qtt);
            ing.unit = bodyIng[i].unit;
            ingList.push(ing);
            ing = {};
        }
        myMongo.getBuyUnitFromName(ing.name, function(buyUnit) {
            ing.buyUnit = buyUnit;
            if(req.body.private) {
                pv = false;
            }

            //creation du document pour insertion dans mongo
            let docNewCourse = {
                name: req.body.course_name,
                creation_date: new Date().getTime(),
                type: parseInt(req.body.type_choice),
                diet: parseInt(req.body.diet_type),
                portion_nbr: parseInt(req.body.portion_nbr),
                ingList: ingList,
                v: app.             getVersion(),
                pv: pv,
                userId: app.getSess().userId,
                recLink: req.body.course_recipe_link
            };

            myMongo.insertDocument(COL_COURSES, docNewCourse, function(result) {
                res.redirect('/newcourse?result=ok')
            });
        })
          .catch(next)
    } else {
        res.redirect('/connect')
    }
};

function addIngredients(req, res, next){
    let ing = {};

    for(let i = 0; i < req.body.ing.length; i++) {
        ing.name = req.body.ing[i][0];
        ing.cat = req.body.ing[i][1];
        ing.unit = req.body.ing[i][2];
        ing.creatorId = app.getSess().userId;
        ing.isValidate = true;
        myMongo.insertDocument(COL_INGREDIENTS, ing, function(result) {
            console.log("Inserted a document into the ingredients collection.");
        })
          .catch(next);
        ing = {};
    }
    res.redirect(app.getSess().redirectTo);
};

function menu(req, res, next) {
    try {
      //noon and evening, starters and/or main course and/or dessert, portions managed
      let duration = parseInt(req.body.create_duration),
        diet = parseInt(req.body.diet_type),
        onlyMine = eval(req.body.onlymine),
        people_nbr = parseInt(req.body.people_nbr),
        //total number of portion equal : nbr of people on menu x number of day of the menu x nbr of service (2 : noon and evening)
        //so : people_nbr*duration*2
        totalPortionNbr = people_nbr * duration * 2,
        requestedTypes = 0,
        coursesArrays,
        mainCoursesPromise = [], startersPromise = [], dessertsPromise = [];

      if (req.body.type_main_course !== undefined) {
        requestedTypes += parseInt(req.body.type_main_course);
        mainCoursesPromise = getCoursesFromDb(onlyMine, diet, parseInt(req.body.type_main_course), people_nbr, totalPortionNbr).catch(next);
      }
      if (req.body.type_starter !== undefined) {
        requestedTypes += parseInt(req.body.type_starter);
        startersPromise = getCoursesFromDb(onlyMine, diet, parseInt(req.body.type_starter), people_nbr, totalPortionNbr).catch(next);
      }
      if (req.body.type_dessert !== undefined) {
        requestedTypes += parseInt(req.body.type_dessert);
        dessertsPromise = getCoursesFromDb(onlyMine, diet, parseInt(req.body.type_dessert), people_nbr, totalPortionNbr).catch(next);
      }
      let startAt = req.body.create_start;

      Promise.all([mainCoursesPromise, startersPromise, dessertsPromise])
        .then(function (values) {
          coursesArrays = {
            mainCourses: values[0],
            starters: values[1],
            desserts: values[2]
          };
          coursesArrays = normalizationOfArrays(coursesArrays, people_nbr, totalPortionNbr);
          coursesArrays.requestedTypes = requestedTypes;

          let sendContent = app.getContent();
          let menu = createMenu(coursesArrays.cloneIt(), startAt, people_nbr);
          //console.log(util.inspect(menu, {depth: null}));
          let datas = {
            coursesArray: JSON.stringify(menu.html),
            ingsList: JSON.stringify(createIngList(coursesArrays.cloneIt())),
            startDate: startAt
          };
          //save menu in database
          let menuObj = {
            obj: menu.obj,
            creation_date: new Date().getTime(),
            creatorId: app.getSess().userId,
            params: {
              onlyMine: onlyMine !== undefined,
              diet: diet,
              duration: duration,
              type_main_course: req.body.type_main_course,
              type_starter: req.body.type_starter,
              type_dessert: req.body.type_dessert,
              people_nbr: people_nbr,
              totalPortionNbr: totalPortionNbr
            }
          };
          myMongo.saveMenu(menuObj, function (err, success) {
            if (!success) console.error("FAIL to save menu : " + util.inspect(menuObj, {depth: null}) + "\nerror : " + err);
          });
          sendContent.data = datas;
          res.render('menu', sendContent);
          app.resetToyostContent();
        })
    } catch(error) {
        console.log("ERROR in menu : ");
        console.log(util.inspect(error));
        next();
    }
}

function resetrequestsent(req, res, next) {
    //save request in db
    myMongo.saveRequest(req.body.emailToReset,function(hash, result) {
        //send mail
        let url = app.getAppUrl()+"/pwdreset?hash="+hash;
        let options = {
            to: req.body.emailToReset,
            subject: app.getTrad().mailing_reset_pwd_subject,
            text: app.getTrad().mailing_reset_pwd_text_1+url+app.getTrad().mailing_reset_pwd_text_2,
            html: serverTools.getMailFromFile("views/mails/resetPwd.handlebars", {url: url, cont: app.getTrad()})
        };
        app.sendMail(options);
        res.redirect("resetrequestsent");
    })
      .catch(next);
}

function resetpwd(req, res, next) {
    //check if hash is valid : exit in db and not older than 24h
    myMongo.isHashValid(req.body.hash, function(hashValid) {
        //if hash valid : continue
        if(hashValid) {
            //check if pwd matches
            if(req.body.pwd === req.body.pwd_confirm) {
                //replace old pwd by new
                myMongo.replacePwd(req.body.pwd, req.body.hash, function(success) {
                    if(success) {
                        //redirect to connect
                        res.redirect("/connect")
                    } else {
                        app.setToyostContent({
                            msg: app.getTrad().toyost_error,
                            timeout: 3000
                        });
                        res.redirect("/pwdreset?hash="+req.body.hash)
                    }
                })
                  .catch(next)
            } else {
                app.setToyostContent({
                    msg: app.getTrad().reset_pwd_dont_match,
                    timeout: 5000
                });
                res.redirect("/pwdreset?hash="+req.body.hash)
            }
        } else {
            app.setToyostContent({
                msg: app.getTrad().reset_hash_not_valid,
                timeout: 5000
            });
            //else redirect to pwdreset page and display toyost error
            res.redirect("/pwdreset?hash="+req.body.hash)
        }
    })
      .catch(next)
}

module.exports = postRequests;

/** PRIVATE */

function starterRequested(types) {
    return types === 7
        || types === 6
        || types === 3
        || types === 2;
}

function mainCourseRequested(types) {
    return types === 7
        || types === 6
        || types === 5
        || types === 4;
}

function dessertRequested(types) {
    return types === 7
        || types === 5
        || types === 3
        || types === 1;
}

function normalizationOfArrays(coursesArrays, people_nbr, totalPortionNbr) {
    //search for smallest portion number : it will be the reference if smaller than totalPortionNbr
    let smallestPortionNbrSum = totalPortionNbr,
        portionNbrSum;
    for(let coursesArraysIndex in coursesArrays) {
        portionNbrSum = coursesArrays[coursesArraysIndex].getPortionSum();
        if(portionNbrSum !== 0 && (portionNbrSum < smallestPortionNbrSum)) {
            smallestPortionNbrSum = portionNbrSum;
        }
    }
    //I want that if one day is start, it has to be finished
    //so max portion number as to be a multiple of people_nbr x the number of service (2 : noon and evening)
     if(smallestPortionNbrSum%(people_nbr*2) !== 0) {
        smallestPortionNbrSum -= smallestPortionNbrSum%(people_nbr*2);
    }

    //normalize all arrays to the same portion nbr
    for(let index in coursesArrays) {
        if(coursesArrays[index].length > 0)
            coursesArrays[index] = normalizePortionNbr(coursesArrays[index].cloneIt(), smallestPortionNbrSum, people_nbr);
    }
    coursesArrays.smallestPortionNbrSum = smallestPortionNbrSum;
    return coursesArrays;
}

function normalizePortionNbr(courseArray, portionNbrSumToReach, people_nbr) {
    //the min and max value for portion number come from getCoursesFromDb portion_nbr filters
    let maxPortionNbr = people_nbr*3, minPortionNbr = people_nbr,
        portionNbrSum,
        normalizedCourseArray = [],
        nextRest, rest, objIndex;

    portionNbrSum = normalizedCourseArray.getPortionSum();
    while(portionNbrSum !== portionNbrSumToReach) {
        rest = portionNbrSumToReach - portionNbrSum;
        //if rest is <= total max portion nbr check if there is a course with this amount of portion
        if(rest <= maxPortionNbr && courseArray.findIndex(function(obj) {
                return obj !== undefined && obj.portion_nbr === rest;
            }) === -1) {
            //if not : they're is no possibity to complete array so move a course from normalizedCourseArray to courseArray to try another configuration
            courseArray.push(normalizedCourseArray.splice(normalizedCourseArray.length - 1, 1)[0]);
            portionNbrSum = normalizedCourseArray.getPortionSum();
        } else {
            //select a random index in course left in courseArray
            objIndex = getRandomOn(courseArray.length);
            //calc what will be the next rest with this index
            nextRest = portionNbrSumToReach - (normalizedCourseArray.getPortionSum() + courseArray[objIndex].portion_nbr);
            //only use this index if next rest === 0 (we reach the portion nbr requested)
            //or if >= minPortionNbr (mean that it is possible that a course with this nbr of portion exist)
            if(nextRest === 0 || nextRest >= minPortionNbr) {
                normalizedCourseArray.push(courseArray.splice(objIndex, 1)[0]);
                portionNbrSum = normalizedCourseArray.getPortionSum();
            }

        }
    }

    return normalizedCourseArray;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getRandomOn(length) {
    let rand = Math.floor(Math.random()*length);
    //console.log("rand : "+rand);
    return rand;
}


function createMenu(coursesArrays, startAt, people_nbr) {
    //the min and max value for portion number come from getCoursesFromDb portion_nbr filters
    let minPortions = people_nbr,
        startDate = new Date(Number(startAt)),
        htmlArray = "",
        menu = makeEmptyMenu(), service = [],
        menuIndex = 0, serviceIndex = "noon",
        day = 1,
        tempCourses, rest;

    //fill empty days
    while(day < startDate.getDay()) {
        htmlArray += makeEmptyCell(day);
        day = (day + 1) % 7;
    }

    while(menuIndex <= 7) {
        tempCourses = {
            mainCourses: [],
            starters: [],
            desserts: []
        };

        //get courses for each type of course
        for(let courseType in coursesArrays) {
            if(coursesArrays[courseType].length > 0) {
                //getFirst course
                tempCourses[courseType].push(coursesArrays[courseType].splice(getFirstCourseIndex(coursesArrays[courseType], day, minPortions), 1)[0]);
                while(tempCourses[courseType].getPortionSum() < minPortions) {
                    //need at least minPortions per service
                    rest = minPortions - tempCourses[courseType].getPortionSum();
                    tempCourses[courseType].push(coursesArrays[courseType].splice(getComplementaryCourseIndex(coursesArrays[courseType], rest, minPortions), 1)[0]);
                }
            }
        }


        //use courses to create menu
        for(let tempCourseType in tempCourses) {
            //need minPortions portions per service
            rest = minPortions;
            for(let i in tempCourses[tempCourseType]) {
                //save course
                menu[menuIndex][serviceIndex][tempCourseType].push({
                    portion_nbr: tempCourses[tempCourseType][i].portion_nbr <= rest ? tempCourses[tempCourseType][i].portion_nbr : rest,
                    name: tempCourses[tempCourseType][i].name,
                    recLink: "",
                    ingList: tempCourses[tempCourseType][i].ingList
                });
                tempCourses[tempCourseType][i].portion_nbr -= rest;
                tempCourses[tempCourseType][i].firstUsedDate = day;
                //if portion negatives : need more portion
                if(tempCourses[tempCourseType][i].portion_nbr < 0) {
                    //update rest
                    rest = Math.abs(tempCourses[tempCourseType][i].portion_nbr);
                }
                //if left portion in courses reintroduce it in course array to pick it later
                if(tempCourses[tempCourseType][i].portion_nbr > 0) {
                    coursesArrays[tempCourseType].push(tempCourses[tempCourseType].splice(i, 1)[0]);
                }
            }
        }

        //service ended
        //set next service and/or change day
        switch(serviceIndex) {
            case "noon":
                serviceIndex = "evening";
                break;
            case "evening":
                //add portions for course
                htmlArray += makeCell(day, menu[menuIndex])
                serviceIndex = "noon";
                day = (day + 1) % 7;
                menuIndex++;
                break;
        }
    }
    //fill end of week
    while(day !== 1) {
        htmlArray += makeEmptyCell(day);
        day = (day + 1) % 7;
    }
    return {html: htmlArray, obj: menu};
}

function makeEmptyMenu() {
    let service = {
        mainCourses: [],
        starters: [],
        desserts: []
    };
    let day = {
            "noon": service.cloneIt(),
            "evening": service.cloneIt()
        };
    let menu = [];

    for(let i = 0; i < 7; i++) {
        menu.push(day.cloneIt());
    }
    return menu;
}

function getFirstCourseIndex(coursesArray, day, minPortions) {
    for(let index in coursesArray) {
        //if course not used yet (lud === undefined) : go
        //if course have been prepared 2 day's ago (day - lud === 2) : go
        //if course have been prepared 1 day's ago (day - lud === 1) but remainings portions > min portion need the two left days to use all portions : go
        //if left only one course (array.length === 1) can't choose anything else so... : go
        if(coursesArray[index].firstUsedDate === undefined
            || day - coursesArray[index].firstUsedDate === 2
            || (day - coursesArray[index].firstUsedDate === 1 && coursesArray[index].portion_nbr >= minPortions)
            || coursesArray.length === 1) {
            return index;
        }
    }
}

function getComplementaryCourseIndex(coursesArray, rest, minPortions) {
    //search for course with the exact number of portion
    let tempIndex = coursesArray.findIndex(obj => obj !== undefined && obj.portion_nbr === rest)
    if(tempIndex > -1) {
        return tempIndex;
    }
    //else if not exact nbr search for nbrPortion % minPortionNbr === rest
    //next selections will ended round
    else {
        tempIndex = coursesArray.findIndex(obj => obj !== undefined && obj.portion_nbr%minPortions === rest)
        if(tempIndex > -1) {
            return tempIndex;
        }
        //else search for course for more portions than need
        //to avoid to have to many course on one service
        else {
            tempIndex = coursesArray.findIndex(obj => obj !== undefined && obj.portion_nbr > rest)
            if(tempIndex > -1) {
                return tempIndex;
            }
            //else take first fud === undefined
            else {
                tempIndex = coursesArray.findIndex(obj => obj !== undefined && obj.firstUsedDate === undefined)
                if(tempIndex > -1) {
                    return tempIndex;
                } else {
                    //else take the course with the greatest portion nbr
                    tempIndex = coursesArray.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0)
                    if(tempIndex > -1) {
                        return tempIndex;
                    }
                }
            }

        }
    }
}

function makeEmptyCell(day) {
    return makeCell(day);
}

function makeCell(day, dayMenu) {
    let cell = "";
    //if monday : open line
    if(day === 1) {
        cell += "<tr>"
    }
    cell += "<td>";
    cell += getNoonTitle();
    if(dayMenu !== undefined) cell += getNoonCellPart(dayMenu["noon"]);
    cell += getEveningTitle();
    if(dayMenu !== undefined) cell += getEveningCellPart(dayMenu["evening"]);
    cell += "</td>";
    //if sunday : close line
    if(day === 0) {
        cell += "</tr>"
    }
    return cell;
}

function getNoonTitle() {
    return getServiceTitle("noon");
}

function getEveningTitle() {
    return getServiceTitle("evening");
}

function getServiceTitle(service) {
    return "<span class='mealLabel'>"+app.getTrad()[service]+" :</span><br />";
}

function getServiceCellPart(dayMenu, service) {
    let cellPart = "";

    if(dayMenu.starters.length > 0) cellPart += makeCourseLines(dayMenu.starters, service, app.getTrad().type_starter);
    if(dayMenu.mainCourses.length > 0) cellPart += makeCourseLines(dayMenu.mainCourses, service, app.getTrad().type_main_course);
    if(dayMenu.desserts.length > 0) cellPart += makeCourseLines(dayMenu.desserts, service, app.getTrad().type_dessert);

    return cellPart;
}

function getNoonCellPart(dayMenu) {
    return getServiceCellPart(dayMenu, "noon")
}

function getEveningCellPart(dayMenu) {
    return getServiceCellPart(dayMenu, "evening")
}

function makeCourseLines(courseArray, service, type) {
    let lineClass = "",
        startFirstLine = "<u>" + type + ":</u> ";
    let courseLine = "";
    for(let course of courseArray) {
        courseLine += "<span class='" + service + " meal "+type+lineClass+"'>" + startFirstLine + course.name + " (<span class='nbrPortion'>"+ course.portion_nbr + "</span> " + app.getTrad().shares+")";
        if (course.recLink !== undefined && course.recLink !== "") {
            courseLine += " (<a href='" + course.recLink + "' target='_blank' title='" + app.getTrad().menu_recipe_link_title + "'>" + app.getTrad().recipe + "</a>)";
        }
        courseLine += "</span><br />";
        lineClass = " secondLine";
        startFirstLine = "";
    }
    return courseLine;
}

/**
 * get list of courses form database.
 * A bunch of n item where n is the total number of portion to be sure that we'll have enough portion even if all courses are set to 1 portion
 * @param onlyMine boolean
 * @param diet int (1, 2, 3)
 * @param type int (1, 2, 4)
 * @param people_nbr int (>= 1)
 * @param totalPortionNbr int
 * @returns {Promise}
 */
function getCoursesFromDb(onlyMine, diet, type, people_nbr, totalPortionNbr) {
    return new Promise(function(resolve, reject) {
        //creation du tableau pour l'aggregate
        //$match : on ne récupère que les objets de la collection qui correspondent aux objets passé en parametre
        let match = {};
        match.$match = {};
        match.$match.$and = [];
        //filtre minimum : les plats non privés (sauf si ce sont ceux de l'utilisateur) : champ $or
        let or = [];
        //plats publique
        or.push({pv: false});
        //ou avec pv non renseigné
        or.push({pv: {$exists: false}});
        if(app.getIsCo()) {
            //ou id createur = id user co (si user co)
            or.push({userId: app.getSess().userId});
            if(onlyMine){
                //if onlyMine required force userId to be equal connected
                match.$match.$and.push({userId: app.getSess().userId});
            }
        }
        match.$match.$and.push({$or: or});
        match.$match.$and.push({diet: {$gte: parseInt(diet)}});
        match.$match.$and.push({type: type});
        //number of portion have to be equal or less to 3 x nbr of people requested for menu
        //because we consider that a course cant be safe after tree days and proposed only once a day
        //so if two people eat on the menu the maximum of portion which will be proposed is 2*1(once a day)*3(3 days max)
        match.$match.$and.push({portion_nbr: {$lte: parseInt(3*people_nbr)}});
        //to maximize the situation where people eat the same thing get only course with at least enough portion for everyone
        match.$match.$and.push({portion_nbr: {$gte: parseInt(people_nbr)}});
        //$sample : get "size element randomly from list get from $match
        let sample = {};
        sample.$sample = {};
        //size of sample will be the total number of portion to be sure that we'll have enough portion even if all courses are set to 1 portion
        sample.$sample.size = totalPortionNbr;
        //on met les pipes dans un tableau
        let aggregate = [match, sample];
        myMongo.aggregate(aggregate, function(err, results){
            if(null !== err) reject(err)
            else resolve(results)
        });
    });
}

function createIngList(normalizedCoursesArrays) {
    let ingredients = {}, units = {};
    let ingId, unitId;
    // iterate over every ingredients of courses
    for(let courseType in normalizedCoursesArrays) {
        if(courseType === "starters" || courseType === "mainCourses" || courseType === "desserts") {
            for(let course of normalizedCoursesArrays[courseType]) {
                    for(let ing of course.ingList) {
                        //convert unit to buy unit
                        ing = normalizeIng(ing.cloneIt());
                        //get usable html id for ing and unit from names
                        ingId = serverTools.createId(ing.name);
                        unitId = serverTools.createId(ing.unit);
                        //if ingredient don't exist yet
                        if(ingredients[ingId] === undefined) {
                            //create unit objects
                            units = {};
                            units[unitId] = {
                                name: ing.unit,
                                qtt: ing.qtt
                            };
                            //add it
                            ingredients[ingId] = {
                                name: ing.name,
                                units: units
                            }
                        } else {
                            // incremente quantity for existing ing
                            if(ingredients[ingId].units[unitId] === undefined) {
                                //if unit don't exist for this ingredient : add it
                                ingredients[ingId].units[unitId] = {
                                    name: ing.unit,
                                    qtt: ing.qtt
                                };
                            } else {
                                //if unit already exist for this ingredient : increment it
                                ingredients[ingId].units[unitId].qtt += ing.qtt;
                            }
                        }
                    }
            }
        }
    }

    //create ing html list and return it
    return createIngHtmlList(ingredients);
}

function createIngHtmlList(ingredients) {
    let ingList = "";
    //loop over ingredients object
    for(let ingIndex in ingredients) {
        ingList += "<div class='ingName' id='"+ingIndex+"'>";
        ingList += "<h4 class='list-group'>"+ingredients[ingIndex].name+" :</h4>";
        for(let unitIndex in ingredients[ingIndex].units) {
            ingList += "<div class='ingQtt list-group-item' id='" + ingIndex + "_" + unitIndex +"'>";
            let quantity = ingredients[ingIndex].units[unitIndex].qtt === null ? "" : ingredients[ingIndex].units[unitIndex].qtt.toFixed(1);
            ingList += "<span id='qtt_" + ingIndex + "_" + unitIndex + "'>" + quantity + "</span> " + ingredients[ingIndex].units[unitIndex].name;
            ingList += "</div>";
        }
        ingList += "</div>";
    }
    return ingList
}

function normalizeIng(ing) {
    if(ing.unit !== ing.buyUnit) {
        let convRate = getConversionRate(ing.unit, ing.buyUnit);
        if(convRate !== undefined) {
            ing.unit = ing.buyUnit;
            ing.qtt = ing.qtt*convRate;
        }
    }
    return ing;
}

function getConversionRate(ingUnit, ingBuyUnit) {
    let convert = app.getConversionObj();
    if(convert[ingUnit] !== undefined) {
        return convert[ingUnit].into[ingBuyUnit];
    }
    return undefined;
}