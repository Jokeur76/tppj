from pymongo import MongoClient
import json

client = MongoClient('localhost', 27017)
db = client['tppj-dev']
raw_recipes = db['raw_recipes']
ingredients = db['ingredients']


test_batch = list()
try:
    # ligne 50
    # 15.7%
    # with open('test_batch_bak.json') as f:
    with open('test_batch.json') as f:
        test_batch = json.load(f)
except json.decoder.JSONDecodeError as jde:
    print("test batch JSONDecodeError")
    print(jde)

units_conversion = {
    "petit pot": "Boîte(s)",
    "pot": "Boîte(s)",
    "g": "Gramme(s)",
    "l": "Litre(s)",
    "cl": "Centilitre(s)",
    "ml": "Millilitre(s)",
    "cuillère à soupe": "Cuillère(s) à soupe",
    "cuillère à café": "Cuillère(s) à café",
    "gousse": "Gousse(s)",
    "pincée": "Pincée(s)",
    "sachet": "Sachet(s)",
    "poignée": "Poignée(s)",
    "verre": "Verre(s)",
    "rouleau": "Unité(s)",
    "zeste": "Un peu",
    "tranche": "Tranche(s)",
    "tranche fine": "Tranche(s)",
    "bûchette": "Unité(s)",
    "pavé": "Unité(s)",
    "carré": "Unité(s)",
    "grosse poignée": "Poignée(s)",
    "petite poignée": "Poignée(s)",
    #"filet": "Un peu",
    "feuille": "Unité(s)",
    "boule": "Unité(s)",
    "portion": "Unité(s)",
    "morceau": "Un peu",
    "paquet": "Paquet(s)",
    "boîte": "Boîte(s)",
    "petite boîte": "Boîte(s)",
    "grande boîte": "Boîte(s)",
    "grosse boîte": "Boîte(s)",
    "boîte moyenne": "Boîte(s)",
    "bouquet": "Bouquet(s)",
    "dé": "Dé(s)",
}


def particular_case(ing_name):
    if ing_name == 'farine':
        return 'farine de blé'
    elif ing_name == 'sucre':
        return 'sucre en poudre'
    elif ing_name == 'sucre semoule':
        return 'sucre en poudre'
    elif ing_name == 'crème fraîche épaisse':
        return 'crème fraîche'
    elif ing_name == 'crème épaisse':
        return 'crème fraîche'
    elif ing_name == 'fromage râpé':
        return 'gruyère râpé'
    elif ing_name == 'cerneau de noix':
        return 'noix'
    elif ing_name == 'crème fleurette':
        return 'crème liquide'
    else:
        return ing_name


def get_unit(ing_name):
    search_unit = None
    rest = 100000
    for marm_unit, my_unit in units_conversion.items():
        # must match for 'g' in ' g de farine' but not in 'fromage'
        # must match cuillère à café dans ' cuillère à café de sucre'
        is_unit = True
        for marm_unit_word in marm_unit.split(' '):
            is_unit = is_unit and marm_unit_word in ing_name.split(' ')
        if is_unit and rest > len(ing_name.replace(marm_unit, '')):
            rest = len(ing_name.replace(marm_unit, ''))
            # save unit
            search_unit = marm_unit

    return search_unit


def cast_ing(ing_list):
    all_succeed = True
    not_formatable = list()
    try:
        with open('not_formattable_ing.json') as f:
            not_formatable = json.load(f)
    except json.decoder.JSONDecodeError as jde:
        print("not_formattable_ing JSONDecodeError")
        print(jde)
    casted_ing_list = list()
    for ing in ing_list:
        unit = None
        current_ing = str(ing['ing'])
        # if ing['qtt'] != "null" and ing['qtt'] is not None:

        unit = get_unit(current_ing)

        # if can find a unit in ing
        if unit is not None:
            # remove unit from ing
            current_ing = current_ing.replace(unit + ' ', '')

            # remove first word of rest
            if current_ing[:2] == "d'":
                current_ing = current_ing[2:]
            else:
                current_ing = ' '.join(current_ing.strip().split(' ')[1:])

        # search for ingredient in db from what left (trimed) in ing
        current_ing = particular_case(current_ing)
        db_ing_list = list(ingredients.find({'$text': {'$search': current_ing.strip(), '$caseSensitive': False}}))

        db_ing = None
        if len(db_ing_list) == 1:
            db_ing = db_ing_list[0]
        elif len(db_ing_list) > 1:
            # convert some know case
            # split current_ing.name
            splitted = current_ing.split(' ')
            rest = 100000
            # for each dbIng
            for db_ing_loop in db_ing_list:
                contain_all = True
                db_ing_name = db_ing_loop["name"].lower()
                # for each part of current_ing.name
                for word in splitted:
                    wordLower = word.lower()
                    # check if db_ing_name contain all word of currentIng.name
                    contain_all = contain_all and (wordLower in db_ing_name or wordLower + 's' in db_ing_name)
                    # remove current word of db_ing_name
                    db_ing_name = db_ing_name.replace(wordLower, '', 1)
                # if all words of currentIng.name are contained in db_ing_name and rest of dbingname is the smallest
                if contain_all and rest > len(db_ing_name):
                    # select this ing
                    db_ing = db_ing_loop
                    # redefine
                    rest = len(db_ing_name)
        # if no ing is selected
        if db_ing is None:
            # save uncastable ing
            exist = False
            for nf in not_formatable:
                if nf == ing:
                    exist = True
            if not exist:
                not_formatable.append(ing)
            all_succeed = False
        # else
        else:
            # return  well formatted ing
            casted_ing_list.append({
                "name": db_ing['name'],
                "qtt": ing['qtt'],
                "unit": unit,
                "buyUnit": db_ing['buyUnit']
            })
    # write uncastable ing in file
    with open('not_formattable_ing.json', 'w') as outfile:
        json.dump(not_formatable, outfile, ensure_ascii=False)
    # if all_succeed:
    return casted_ing_list
    # else:
    #     return None


with open('not_formattable_ing.json', 'w') as outfile:
    json.dump(list(), outfile, ensure_ascii=False)
cast_ing_list = cast_ing(test_batch)
if len(cast_ing_list) != len(test_batch):
    percent = len(cast_ing_list) / len(test_batch) * 100
    print(str(percent) + "% success")
    # for ingredient in cast_ing_list:
    #     print(ingredient)
else:
    print('gooood')
