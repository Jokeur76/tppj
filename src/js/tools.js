function createId(str) {
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
        /'/g, / /g, /’/g, '(', ')', /,/g, /\./g
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c','_','_','_', '', '', '', ''];

    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }

    return str;
}

function toyoster(msg, timeout) {
    var toyost = $('#toyost');
    var content = $('#toyost_msg');
    content.html(msg);
    //TODO revoir la gestion de la taille du toyost
    if(msg.length > 70) {
        toyost.addClass("twoLines")
    }
    //set default timeout
    if(!timeout) {
        timeout = 3000;
    }
    toyost.show();
    setTimeout(function(){toyost.hide();}, timeout);
}

function displayToyost() {
    $('#toyost').hide();
    if(toyostObj.msg != "") {
        toyoster(toyostObj.msg, toyostObj.timeout);
    }
}

function redirect(url) {
    window.location = url;
}

function getIngForSelect(id, selectIdBasis) {
    return new Promise(function(resolve, reject) {
        var getIng = $.get('/get_ing');
        getIng.done(function(response){
            var selectIdBasis = "ingredients";
            var id = selectIdBasis + ($('.ingSelect').length);
            $('.ingSelect:last').attr('id', id + 'Div');
            $('.ingSelect:last .ingName').attr('id', id);
            $('.ingSelect:last .ingName').attr('name', 'ing['+($('.ingSelect').length-1)+'][name]');
            $('.ingSelect:last .ingQtt').attr('name', 'ing['+($('.ingSelect').length-1)+'][qtt]');
            $('.ingSelect:last .ingUnit').attr('name', 'ing['+($('.ingSelect').length-1)+'][unit]');
            $('.ingSelect:last .btn-primary').on('click', function () {
                removeIng(id + 'Div')
            });

            $('#'+id).empty();
            console.log('getIngForSelect')
            var selectNbr = id.substr(selectIdBasis.length);
            var cat = "";
            var ingCat = "";
            var optGroup = null;
            var option = null;
            var j = 0;
            for(var i = 0; i < response.length; i++) {
                ingCat = createId(response[i].cat);
                ingCat = ingCat+selectNbr;
                if(document.getElementById(ingCat) == null) {
                    optGroup = null;
                    optGroup = $('<optgroup id="'+ingCat+'">');
                    optGroup.attr('label', response[i].cat);
                    $('#'+id).append(optGroup)
                }

                option = $("<option></option>");
                option.val(response[i].name);
                option.text(response[i].name);
                option.attr('rel', response[i].unit);
                $('#'+ingCat).append(option);
            }

            $('#'+id).select2();
            resolve();
        }, function(err) {
            reject(err);
        })
    })
}

function getUnitForSelect() {
    return new Promise(function(resolve, reject) {
        var selectIdBasis = "units";
        var id = selectIdBasis+($('.ingSelect').length);
        $('.ingSelect:last .ingUnit').attr('id', id);
        var getUnit = $.get('/get_unit');
        getUnit.done(function(response){
            $('#'+id).empty();
            var option = null;
            for(var i = 0; i < response.length; i++) {
                option = $("<option></option>");
                option.val(response[i].name);
                option.text(response[i].name)
                $('#'+id).append(option);
            }

            $('#'+id).select2();
            resolve("Stuff worked!");
        }, function(err) {
            reject(Error(err));
        });

    });
}

function getCatForSelect(id) {
    $.get('/get_cat', function(response){
        $('#'+id).empty();
        var option = null;
        for(var i = 0; i < response.length; i++) {
            option = $("<option></option>");
            option.val(response[i].name);
            option.text(response[i].name)
            $('#'+id).append(option);
        }

        $('#'+id).select2();
    })
}

function addIngTemplate() {
    console.log('addIngTemplate');
    $('#ingPanel').append(selectTemplate);
}

function addCatList() {
    var selectIdBasis = "cats";
    var id = selectIdBasis+($('.ingSelect').length+1);
    $('.ingSelect:last .ingCat').attr('id', id);
    getCatForSelect(id, selectIdBasis);
}

function getUrlParam(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}