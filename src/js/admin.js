$(function() {
    if(window.location.pathname === "/admin") {
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: '/isAdmin',
            success: function (isAdmin) {
                if (!isAdmin) {
                    console.warn("unauthorized user try to connect to admin page. Send alert to admin.");
                    redirect("/logout");
                }
            }
        });
        addIng();
    }
});

function addIng() {
    addIngTemplate();
    getUnitForSelect();
    addCatList();
}