$(function() {
    $.datepicker.setDefaults($.datepicker.regional['fr']);
    displayToyost();
    if(isCo) {
        $('#nav-list').append(courseLink)
                      .append(logoutLink);
    } else {
        $('#nav-list').append(loginLink)
                      .append(registerLink)
    }

    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: '/isAdmin',
        success: function(isAdmin) {
            if(typeof isAdmin === "boolean" && isAdmin) {
                $('#nav-list').append(adminLink);
            } else {
                console.log("user is not admin");
                console.log(isAdmin);
            }
        },
        error: function(err) {
            console.log("error in get is admin : ");
            console.log(err);
        }
    });

    $( ".tooltips-help" )
        .mouseover(function() {
            $('.tooltips').removeClass("hide");
        })
        .mouseout(function() {
            $('.tooltips').addClass("hide");
        });

});