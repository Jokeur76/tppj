global.__base = __dirname.replace('tests', '');
global.__env = "integration-tests";

var app = require(global.__base+'app.js');
const jsdom = require("jsdom");
const util = require("util");
const { JSDOM } = jsdom;
var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

var postRequests = require(global.__base+'modules/postRequests.js');
var myMongo = require(global.__base+'modules/myMongo.js')

var req, res, next, count_before, datas, count_after, testPossible;

describe('menu', function() {
    var people_nbr = 2, duration = 7;
    before(function(done) {
        // count nbr of plat to be sure that test can pass
        myMongo.countCollection("courses", function(err, count_courses) {
            if (count_courses > 14) {
                testPossible = true;
                //count nbr of menu
                myMongo.countCollection("menu", function(err, _count_before) {
                    assert.equal(err, null);
                    count_before = _count_before;
                    //mock response and request
                    req = {
                        body: {
                            "diet_type": "1",
                            "type_starter": "2",
                            "type_main_course": "4",
                            "type_dessert": "1",
                            "create_duration": duration.toString(),
                            "create_start": new Date().getTime(),
                            "people_nbr": people_nbr.toString()
                        }
                    };
                    res = {};
                    next = function(err) {console.log('next')};
                    app.setContent({cont: app.getTrad(), toyostMsg: "", toyostTimeout: "", isCo: false})
                    res.render = function(path, content) {
                        datas = content.data;
                        myMongo.countCollection("menu", function(err, _count_after) {
                            assert.equal(err, null);
                            count_after = _count_after;
                            done()
                        })
                    };
                    postRequests.menu(req, res, next);
                })
            } else {
                console.log("INTEGRATIONS TESTS NOT POSSIBLE BECAUSE NOT ENOUGH COURSES");
                testPossible = false;
                done();
            }
        })
    });

    it('should save a json menu in database', function(done) {
        if (testPossible) {
            assert.equal(count_before + 1, count_after);
            done();
        } else {
            done();
        }
    });

    it('should return an omnivore html menu in response for 2 people, 7 days with starters, main course and desserts starting today', function(done) {
        if (testPossible) {
            const dom = new JSDOM(datas.coursesArray);
            let sum = 0;
            let test = dom.window.document.querySelectorAll('.noon.meal.' + app.getTrad().type_starter + ' .nbrPortion');
            for (let entry1 of test) {
                sum += parseInt(entry1.textContent);
            }
            assert.equal(sum, people_nbr * duration);
            sum = 0;
            test = dom.window.document.querySelectorAll('.noon.meal.' + app.getTrad().type_main_course + ' .nbrPortion');
            for (let entry2 of test) {
                sum += parseInt(entry2.textContent);
            }
            assert.equal(sum, people_nbr * duration);
            sum = 0;
            test = dom.window.document.querySelectorAll('.noon.meal.' + app.getTrad().type_dessert + ' .nbrPortion');
            for (let entry3 of test) {
                sum += parseInt(entry3.textContent);
            }
            assert.equal(sum, people_nbr * duration);
            done();
        } else {
            done();
        }
    });

    it('should return an ingList with at least one ingredient and one qtt of one unit', function(done) {
        if (testPossible) {
            const dom = new JSDOM(datas.ingsList);
            expect(dom.window.document.querySelectorAll('.ingName').length).to.be.above(1);
            expect(dom.window.document.querySelectorAll('.ingQtt').length).to.be.above(1);
            var qttId = '#qtt_' + dom.window.document.querySelectorAll('.ingQtt')[0].id;
            assert.equal(isNaN(parseFloat(dom.window.document.querySelector(qttId).textContent)), false);
            done();
        } else {
            done();
        }
    });
});

