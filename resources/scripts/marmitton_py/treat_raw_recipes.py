# TARGET

# {
#     "_id" : ObjectId("5aa3a859bd93afe83833527a"),
#     "name" : "Jalousie au fromage",
#     "creation_date" : 1520674905300,
#     "type" : 2,
#     "diet" : 2,
#     "portion_nbr" : 6,
#     "ingList" : [
#         {
#             "name" : "pâte feuilletée",
#             "qtt" : 2,
#             "unit" : "Unité(s)",
#             "buyUnit" : "Unité(s)"
#         }
#     ],
#     "beta" : true,
#     "pv" : false,
#     "userId" : "marmitton",
#     "recLink" : "http://www.marmiton.org/recettes/recette_jalousie-au-fromage_50278.aspx",
#     "difficulty" : 2,
#     "budget" : 1,
#     "valid" : true
# }

# RAW

# {
#     "_id" : ObjectId("5b0c52206d6bff1a38ff9698"),
#     "title" : "Emincé de porc caracajou ( caramel et noix de cajou ) (13ème rencontre)",
#     "nb_pers" : "6",
#     "tps_prep" : "30 min",
#     "tps_cuisson" : "\r\n\t\t\t\t\t\t\t30 min\t\t\t\t\t\t",
#     "budget" : "assez cher",
#     "difficulte" : "facile",
#     "nbr_portion" : "6",
#     "ing_list" : [
#         {
#             "qtt" : "4",
#             "ing" : " tomate"
#         }
#     ],
#     "tags" : [
#         "Entrée"
#     ]
# }

# {
#     "_id" : ObjectId("5aa3a859bd93afe83833527a"),
#     "name" : raw.title,
#     "creation_date" : int(time.time()*1000),
#     "type" : getType(),
#     "diet" : getDiet(),
#     "portion_nbr" : 6,
#     "ingList" : [
#         {
#             "name" : "pâte feuilletée",
#             "qtt" : 2,
#             "unit" : "Unité(s)",
#             "buyUnit" : "Unité(s)"
#         }
#     ],
#     "beta" : true,
#     "pv" : false,
#     "userId" : "marmitton",
#     "recLink" : "http://www.marmiton.org/recettes/recette_jalousie-au-fromage_50278.aspx",
#     "difficulty" : 2,
#     "budget" : 1,
#     "valid" : true
# }

import time
import re
from pymongo import MongoClient
import bson
import json

start = time.time()
client = MongoClient('localhost', 27017)
db = client['tppj-dev']
raw_recipes = db['raw_recipes']
ingredients = db['ingredients']
units = db['units']
courses = db['courses']

try:
    with open('../../../package.json') as pj:
        package_json = json.load(pj)
except FileNotFoundError as fnfe:
    print("package.json not found")
    print(fnfe)
except json.decoder.JSONDecodeError as jde:
    print("package.json JSONDecodeError")
    print(jde)

units_conversion = {
    "petit pot": "Boîte(s)",
    "pot": "Boîte(s)",
    "g": "Gramme(s)",
    "l": "Litre(s)",
    "cl": "Centilitre(s)",
    "ml": "Millilitre(s)",
    "cuillère à soupe": "Cuillère(s) à soupe",
    "cuillère à café": "Cuillère(s) à café",
    "gousse": "Gousse(s)",
    "pincée": "Pincée(s)",
    "sachet": "Sachet(s)",
    "poignée": "Poignée(s)",
    "verre": "Verre(s)",
    "rouleau": "Unité(s)",
    "zeste": "Un peu",
    "tranche": "Tranche(s)",
    "tranche fine": "Tranche(s)",
    "bûchette": "Unité(s)",
    "pavé": "Unité(s)",
    "carré": "Unité(s)",
    "grosse poignée": "Poignée(s)",
    "petite poignée": "Poignée(s)",
    "feuille": "Unité(s)",
    "boule": "Unité(s)",
    "portion": "Unité(s)",
    "morceau": "Un peu",
    "paquet": "Paquet(s)",
    "boîte": "Boîte(s)",
    "petite boîte": "Boîte(s)",
    "grande boîte": "Boîte(s)",
    "grosse boîte": "Boîte(s)",
    "boîte moyenne": "Boîte(s)",
    "bouquet": "Bouquet(s)",
    "dé": "Dé(s)",
}


def particular_case(ing_name):
    if ing_name == 'farine':
        return 'farine de blé'
    elif ing_name == 'sucre':
        return 'sucre en poudre'
    elif ing_name == 'sucre semoule':
        return 'sucre en poudre'
    elif ing_name == 'crème fraîche épaisse':
        return 'crème fraîche'
    elif ing_name == 'crème épaisse':
        return 'crème fraîche'
    elif ing_name == 'fromage râpé':
        return 'gruyère râpé'
    elif ing_name == 'cerneau de noix':
        return 'noix'
    elif ing_name == 'crème fleurette':
        return 'crème liquide'
    else:
        return ing_name


def get_raw_recipes_list():
    return raw_recipes.find()


def get_type(tags):
    for tag in tags:
        if tag == "Entrée":
            return 2
        elif tag == "Dessert":
            return 1
        elif tag == "Plat principal":
            return 4


def get_diet(tags, ingredients):
    for tag in tags:
        if tag == "Végétarien":
            return 2
        elif tag == "Recettes vegan":
            return 3
    diet = 3
    for ing in ingredients:
        if ing['diet'] == 1:
            diet = 1
        elif ing['diet'] == 2 and diet != 1:
            diet = 2
    return diet


def get_unit(ing_name):
    search_db_unit = None
    search_unit = None
    rest = 100000
    for marm_unit, my_unit in units_conversion.items():
        # must match for 'g' in ' g de farine' but not in 'fromage'
        # must match cuillère à café dans ' cuillère à café de sucre'
        is_unit = True
        for marm_unit_word in marm_unit.split(' '):
            is_unit = is_unit and marm_unit_word in ing_name.split(' ')
        if is_unit and rest > len(ing_name.replace(marm_unit, '')):
            rest = len(ing_name.replace(marm_unit, ''))
            # save unit
            search_unit = marm_unit
            search_db_unit = my_unit

    return search_unit, search_db_unit


def cast_qtt(qtt):
    if qtt == '1/8':
        return 0.125
    if qtt == '1/4':
        return 0.25
    if qtt == '1/3':
        return 0.33
    if qtt == '1/2':
        return 0.5
    if qtt == '2/3':
        return 0.66
    if qtt == '3/4':
        return 0.75
    if qtt is None:
        return None
    return float(qtt)


def cast_ing(ing_list):
    all_succeed = True
    not_formatable = list()
    choosed_db_unit = None
    try:
        with open('not_formattable_ing.json') as f:
            not_formatable = json.load(f)
    except json.decoder.JSONDecodeError as jde:
        print("not_formattable_ing JSONDecodeError")
        print(jde)
    casted_ing_list = list()
    for ing in ing_list:
        unit = None
        current_ing = str(ing['ing'])
        # if ing['qtt'] != "null" and ing['qtt'] is not None:

        unit_tuple = get_unit(current_ing)
        unit = unit_tuple[0]
        choosed_db_unit = unit_tuple[1]

        if choosed_db_unit is None:
            choosed_db_unit = "Un peu" if ing['qtt'] is None else "Unité(s)"

    # if can find a unit in ing
        if unit is not None:
            # remove unit from ing
            current_ing = current_ing.replace(unit + ' ', '')

            # remove first word of rest
            if current_ing[:2] == "d'":
                current_ing = current_ing[2:]
            else:
                current_ing = ' '.join(current_ing.strip().split(' ')[1:])

        # search for ingredient in db from what left (trimed) in ing
        current_ing = particular_case(current_ing)
        db_ing_list = list(ingredients.find({'$text': {'$search': current_ing.strip(), '$caseSensitive': False}}))

        db_ing = None
        if len(db_ing_list) == 1:
            db_ing = db_ing_list[0]
        elif len(db_ing_list) > 1:
            # convert some know case
            # split current_ing.name
            splitted = current_ing.split(' ')
            rest = 100000
            # for each dbIng
            for db_ing_loop in db_ing_list:
                contain_all = True
                db_ing_name = db_ing_loop["name"].lower()
                # for each part of current_ing.name
                for word in splitted:
                    wordLower = word.lower()
                    # check if db_ing_name contain all word of currentIng.name
                    contain_all = contain_all and (wordLower in db_ing_name or wordLower + 's' in db_ing_name)
                    # remove current word of db_ing_name
                    db_ing_name = db_ing_name.replace(wordLower, '', 1)
                # if all words of currentIng.name are contained in db_ing_name and rest of dbingname is the smallest
                if contain_all and rest > len(db_ing_name):
                    # select this ing
                    db_ing = db_ing_loop
                    # redefine
                    rest = len(db_ing_name)
        # if no ing is selected
        if db_ing is None or choosed_db_unit is None:
            # save uncastable ing
            exist = False
            for nf in not_formatable:
                if nf == ing:
                    exist = True
            if not exist:
                not_formatable.append(ing)
            all_succeed = False
        # else
        else:
            # return  well formatted ing
            casted_ing_list.append({
                "name": db_ing['name'],
                "diet": int(db_ing['diet']),
                "qtt": cast_qtt(ing['qtt']),
                "unit": choosed_db_unit,
                "buyUnit": db_ing['buyUnit']
            })
    # write uncastable ing in file
    with open('not_formattable_ing.json', 'w') as outfile:
        json.dump(not_formatable, outfile, ensure_ascii=False)
    if all_succeed:
        return casted_ing_list
    else:
        return None


def get_budget(budget):
    if budget == "bon marché":
        return 1
    elif budget == "Coût moyen":
        return 2
    elif budget == "assez cher":
        return 3
    return 0


def get_difficulty(difficulty):
    if difficulty == "très facile":
        return 1
    elif difficulty == "facile":
        return 2
    elif difficulty == "Niveau moyen":
        return 3
    elif difficulty == "difficile":
        return 4
    return 0


def valid_recipe(recipe):
    valid = True

    # "name" : is not null, is string, is not empty
    valid = valid and recipe['name'] is not None and isinstance(recipe['name'], str) and recipe['name'] != ''
    if not valid:
        return valid, "name"

    # "creation_date" : is not null, is number, length > 13
    valid = valid and recipe['creation_date'] is not None and isinstance(recipe['creation_date'], int) and recipe['creation_date'] >= time.time()
    if not valid:
        return valid, "creation_date"

    # "type" : is not null, is number, length == 1
    valid = valid and recipe['type'] is not None and isinstance(recipe['type'], int) and len(str(recipe['type'])) == 1
    if not valid:
        return valid, "type"

    # "diet" : is not null, is number, length == 1
    valid = valid and recipe['diet'] is not None and isinstance(recipe['diet'], int) and len(str(recipe['diet'])) == 1
    if not valid:
        return valid, "diet"

    # "portion_nbr" : is not null, is number, length == 1
    valid = valid and recipe['portion_nbr'] is not None and isinstance(recipe['portion_nbr'], int)
    if not valid:
        return valid, "portion_nbr"

    # for "ingList" :
    #     "name" : is not null, is string, is not empty
    #     "qtt" : is not null, is number
    #     "unit" : is not null, is string, is not empty, is real unit
    for cur_ing in recipe['ingList']:
        valid = valid and cur_ing['name'] is not None and isinstance(cur_ing['name'], str) and cur_ing['name'] != ''
        if not valid:
            return valid, "ing name"
        valid = valid and (cur_ing['qtt'] is None or (isinstance(cur_ing['qtt'], float) and cur_ing['qtt'] >= 0))
        if not valid:
            return valid, "ing qtt"
        valid = valid and cur_ing['unit'] is not None and isinstance(cur_ing['unit'], str) and cur_ing['unit'] != ''
        if not valid:
            return valid, "ing unit"
        cur_unit = list(units.find({"name": cur_ing['unit']}))
        valid = valid and len(cur_unit) >= 1
        if not valid:
            return valid, "ing real unit"

    # "v" : is not null is string, == "1.0.0",
    # print(package_json['version'])
    valid = valid and recipe['v'] is not None and isinstance(recipe['v'], str) and recipe['v'] == package_json['version']
    if not valid:
        return valid, "version"

    # "pv" : is not null is boolean is false,
    valid = valid and recipe['pv'] is not None and isinstance(recipe['pv'], bool) and recipe['pv'] is False
    if not valid:
        return valid, "pv"

    # "userId" : is not null, is string, == "marmitton_py",
    valid = valid and recipe['userId'] is not None and isinstance(recipe['userId'], str) and recipe['userId'] == "marmitton_py"
    if not valid:
        return valid, "userId"

    # "recLink" : is not null, is string, regex for marmiton url,
    valid = valid and recipe['recLink'] is not None and isinstance(recipe['recLink'], str) and recipe['recLink'] != "" # and re.match("http:\/\/www\.marmiton\.org\/recettes\/[0-9a-z_-]*\.aspx", recipe['recLink'])
    if not valid:
        return valid, "recLink"

    return valid, ""


recipes = 0
loop = 0
for doc in get_raw_recipes_list():
    loop += 1
    try:
        ing_list = cast_ing(doc['ing_list'])
        if ing_list is not None:
            new_recipe = {
                "name": doc['title'],
                "creation_date": int(time.time()*1000),
                "type": get_type(doc['tags']),
                "diet": get_diet(doc['tags'], ing_list),
                "portion_nbr": int(doc['nb_pers']),
                "ingList": ing_list,
                "beta": True,
                "pv": False,
                "userId": "marmitton_py",
                "recLink": doc['url'],
                "v": package_json['version'],
                "difficulty": get_difficulty(doc['difficulte']),
                "budget": get_budget(doc['budget']),
            }
            valid_tuple = valid_recipe(new_recipe)
            new_recipe["valid"] = valid_tuple[0]
            former_recipe = courses.find_one({"recLink": new_recipe['recLink']})
            if former_recipe is not None:
                new_recipe['_id'] = former_recipe['_id']
            if new_recipe["valid"]:
                courses.save(new_recipe)
                recipes += 1
            else:
                print("recipe unvalid because of : " + valid_tuple[1])
        # else:
            # print(doc)
        if loop % 500 == 0:
            print(str(recipes) + '/' + str(loop) + ' recipes saved')
    except KeyError as ke:
        print('KeyError')
        print(str(ke))
    except bson.errors.InvalidDocument as bid:
        print('InvalidDocument')
        print(str(bid))
    except ValueError as ve:
        print('ValueError')
        print(str(ve))
print(str(recipes) + '/' + str(loop) + ' recipes saved')

tps = time.time() - start
secondes = tps % 60
minutes = tps // 60
print('execution in ' + str(minutes) + ' min and ' + str(secondes) + 'secondes')
