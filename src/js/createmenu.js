$(function(){
    if(window.location.pathname === "/createmenu") {
        displayToyost();
        $('#create_start').datepicker({ dateFormat: 'dd-mm-yy', changeYear: true, defaultDate: new Date()})
            .datepicker('setDate', new Date);
        $("#hidden_create_start").val($("#create_start").datepicker( 'getDate' ).getTime());
        $("#create_start").on('change', function() {
            $("#hidden_create_start").val($("#create_start").datepicker( 'getDate' ).getTime());
            console.log($("#hidden_create_start").val())
        });
        if(isCo) {
            $('#params').append(onlyMine);
        }
        $('#order_menu_btn').click(checkMenuForm);
    }
});