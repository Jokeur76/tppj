let getPages = {
    home: home,
    register: register,
    connect: connect,
    createMenu: createMenu,
    newCourse: newCourse,
    admin: admin,
    reset: reset,
    resetrequestsent: resetrequestsent,
    pwdreset: pwdreset
};

let app = require(global.__base+'app.js');
let security = require(global.__base+'/modules/security.js');

let needCoPagesArray = ["newcourse"];

function home(req, res){
    getPage(req, res, "home");
}

function register(req, res){
    getPage(req, res, "register");
}

function connect(req, res){
    getPage(req, res, "connect");
}

function createMenu(req, res){
    getPage(req, res, "createmenu");
}

function newCourse(req, res){
    getPage(req, res, "newcourse");
}

function admin(req, res){
    getPage(req, res, "admin");
}

function reset(req, res){
    getPage(req, res, "reset");
}

function resetrequestsent(req, res){
    getPage(req, res, "resetrequestsent");
}

function pwdreset(req, res){
    let calledPage = "pwdreset";
    //check if hash still valid
    myMongo.isHashValid(req.originalUrl.replace("/pwdreset?hash=",""), function(hashValid) {
        if(!hashValid) {
            calledPage = "pwdresetinvalid";
        }
        getPage(req, res, calledPage);
    })
}

/** PRIVATE */

let getPage = function (req, res, calledPage){
    //set redirectTo to this page to be able to return to it when send post
    app.setRedirectTo(calledPage === "home" ? "/" : "/" + calledPage);
    if(security.isPageSecured(calledPage) && !security.isUserAdmin()) {
        res.render('unauthorized', app.getContent());
    } else if(needCoPagesArray.contains(calledPage) && !app.getIsCo()) {
        res.redirect("/connect");
    } else {
        res.render(calledPage, app.getContent());
    }
    app.resetToyostContent();
};

module.exports = getPages;