module.exports = {
    isPageSecured: isPageSecured,
    isUserAdmin: isUserAdmin
}

var app = require(global.__base+'app.js');

var securedPagesArray = ["admin"];

function isPageSecured(page) {
    return securedPagesArray.contains(page)
};

function isUserAdmin() {
    return app.getSess().isAdmin;
};