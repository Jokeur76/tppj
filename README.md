#BUT

Ceci est un projet d'appli web pour générer des menus en quelques clics à partir de :
- plats ajoutés par l'utilisateur (notamment l'administrateur)
- plats partagés par la communauté

#PREREQUIS
- installer node 6.x.x (ou plus), npm, git, mongoDB, g++
- installer globalement node-gyp, node-sass, mocha, forever, nodemon (`(sudo) npm install xxxxxxx -g`)
- Pour gitBash (windows)
Add the following line to your ~\.bash_profile file. If it does not exist, create it.
PATH="/usr/local/share/npm/bin:/usr/local/bin:/usr/local/sbin:~/bin:$PATH"
- WINDOWS seulement : installer windows-build-tools en tant qu'administrateur (`npm install --global --production windows-build-tools`)

#INSTALLATION
- fork le repo
- se mettre sur la branche develop (`git checkout develop`)
- installer les packages : `npm install`
- générer des plats génériques pour les tests en lancant la commande : `node ./resources/scripts/generateCourses.js run` 
- tester avec 
    - windows
        - `npm run start:debug` dans un premier terminal
        - `npm run end-to-end-tests` dans un second une fois que le premier script est terminé
    - unix
        - `npm run test`
- unix : lancer avec `npm run start:debug`
- se connecter sur localhost:5000
