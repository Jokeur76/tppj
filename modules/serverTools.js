let handlebars = require('handlebars');

let tools = {
    createId: createId,
    isHexaValid:isHexaValid,
    getMailFromFile: getMailFromFile,
    saltHashPassword: saltHashPassword,
    checkPwd: checkPwd
};

function createId(str) {
    if(typeof str === 'string') {
      let accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
        /'/g, / /g, /’/g, '(', ')', /,/g, /\./g
      ];
      let noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c', '_', '_', '_', '', '', '', ''];

      for (let i = 0; i < accent.length; i++) {
        str = str.replace(accent[i], noaccent[i]);
      }
    }

    return str;
}

function isHexaValid(hexa) {
    let checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
    return checkForHexRegExp.test(hexa);
}

function getMailFromFile(path, context) {
    return handlebars.compile(fs.readFileSync(global.__base+path, 'utf8'))(context)
}

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
let genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0,length);   /** return required number of characters */
};

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
let sha512 = function(password, salt){
    let hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};

function saltHashPassword(userpassword, salt) {
    if(salt === undefined) {
        salt = genRandomString(16);
    }
    /** Gives us salt of length 16 */
    return sha512(userpassword, salt);
}

function checkPwd(pwdAttempt, hash, salt) {
    let pwdData = saltHashPassword(pwdAttempt, salt);
    return pwdData.passwordHash === hash;
}

module.exports = tools;