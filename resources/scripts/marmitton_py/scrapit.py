import scrapy
import time
import random
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['tppj-dev']
raw_recipes = db['recipes-raw']

# db['raw_recipes'].find_one_and_update({'count': {'$exists': True}}, {'$set': rv})

# randomRecipeUrl = 'https://www.cuisineaz.com/recettes/mini-gratin-aux-haricots-verts-et-au-poulet-87595.aspx'
randomRecipeUrl = 'http://www.marmiton.org/recettes/recette-hasard.aspx'


# conda update -n base conda


# marmitton
class MarmittonSpider(scrapy.Spider):
    name = "marmitton_spider"
    base_url = 'http://www.marmiton.org'
    type_params = '&dt=dessert&dt=entree&dt=platprincipal'
    search_url = 'http://www.marmiton.org/recettes/recherche.aspx?aqt='
    mot_cles = ['fromage', 'viande', 'poisson', 'lait', 'legume', 'fruit']
    mot_cle_index = 0
    pagination = '&start='
    page = 0
    start_urls = [search_url + mot_cles[mot_cle_index] + type_params + pagination + str(page*12)]
    custom_settings = {}
    test = {}
    ingredients = []
    tags = []

    def parse(self, response):
        RECIPE_SELECTOR = '.recipe-results a.recipe-card ::attr(href)'
        for recipe in response.css(RECIPE_SELECTOR).extract():
            yield scrapy.Request(
                response.urljoin(self.base_url + recipe),
                callback=self.parse_recipe
            )
        NEXT_SELECTOR = '.af-pagination li.next-page'
        next = response.css(NEXT_SELECTOR).extract_first()
        if next is None:
            stat = db['raw_recipes'].find_one({'count': {'$exists': True}})
            try:
                stat.done_list = stat.done_list.push(self.mot_cles[self.mot_cle_index])
            except ValueError as ve:
                print(ve)
            except AttributeError as ae:
                print('AttributeError during DoneList update : ' + ae)
                print('Done keyWord : ' + self.mot_cles[self.mot_cle_index])
                print(ae)
            db['raw_recipes'].find_one_and_update({'count': {'$exists': True}}, {'$set': stat})
            self.mot_cle_index += 1
        self.page += 1
        url = self.search_url + self.mot_cles[self.mot_cle_index] + self.type_params + self.pagination + str(self.page*12)
        yield scrapy.Request(
            response.urljoin(url),
            callback=self.parse
        )

    def parse_recipe(self, response):
        self.parse_page(response)

    def parse_page(self, response):
        # print('#############################################')
        # print(response.url)
        # print('#############################################')
        self.test = {}
        try:
            SET_SELECTOR = '#content'
            for brickset in response.css(SET_SELECTOR):
                TITLE_SELECTOR = 'h1.main-title  ::text'
                NB_PERS_SELECTOR = ".recipe-infos .title-2.recipe-infos__quantity__value ::text"
                TPS_PREP_SELECTOR = ".recipe-infos__timmings .recipe-infos__timmings__preparation .recipe-infos__timmings__value ::text"
                TPS_CUISSON_SELECTOR = ".recipe-infos__timmings__detail .recipe-infos__timmings__cooking .recipe-infos__timmings__value ::text"
                BUDGET_SELECTOR = ".recipe-infos .recipe-infos__budget .recipe-infos__item-title ::text"
                DIFFICULTE_SELECTOR = ".recipe-infos .recipe-infos__level .recipe-infos__item-title ::text"
                NBR_PORTION_SELECTOR = ".recipe-ingredients__qt-counter .recipe-ingredients__qt-counter__value ::attr(value)"
                test = {
                    'url': response.url,
                    'title': brickset.css(TITLE_SELECTOR).extract_first().strip() if brickset.css(TITLE_SELECTOR).extract_first() is not None else "",
                    'nb_pers': brickset.css(NB_PERS_SELECTOR).extract_first(),
                    'tps_prep': brickset.css(TPS_PREP_SELECTOR).extract_first().strip() if brickset.css(TPS_PREP_SELECTOR).extract_first() is not None else "",
                    'tps_cuisson': brickset.css(TPS_CUISSON_SELECTOR).extract_first().strip() if brickset.css(TPS_CUISSON_SELECTOR).extract_first() is not None else "",
                    'budget': brickset.css(BUDGET_SELECTOR).extract_first().strip() if brickset.css(BUDGET_SELECTOR).extract_first() is not None else "",
                    'difficulte': brickset.css(DIFFICULTE_SELECTOR).extract_first().strip() if brickset.css(DIFFICULTE_SELECTOR).extract_first() is not None else "",
                    'nbr_portion': brickset.css(NBR_PORTION_SELECTOR).extract_first()
                }
                ING_SELECTOR = "ul.recipe-ingredients__list li.recipe-ingredients__list__item"
                self.ingredients = []
                for ingredient in brickset.css(ING_SELECTOR):
                    self.ingredients.append({
                        'qtt': ingredient.css(".recipe-ingredient-qt ::text").extract_first(),
                        'ing': ingredient.css(".ingredient ::text").extract_first()
                    })
                test['ing_list'] = self.ingredients
                TAGS_SELECTOR = ".mrtn-tags-list li.mrtn-tag ::text"
                self.tags = []
                for ingredient in brickset.css(TAGS_SELECTOR).extract():
                    self.tags.append(ingredient)
                test['tags'] = self.tags
                raw_recipes = db['raw_recipes']
                db_recipe = raw_recipes.find_one({"title": test["title"]})
                if db_recipe is None:
                    db_recipe = raw_recipes.insert_one(test).inserted_id
                else:
                    raw_recipes.find_one_and_update({'count': {'$exists': True}}, {'$inc': {'count': 1}})
                # yield test
                time.sleep(random.randint(0, 5))
        except ValueError as ve:
            raw_recipes.find_one_and_update({'count': {'$exists': True}}, {'$inc': {'ExceptCount:': 1}})
            print(ve)
        except AttributeError as ae:
            raw_recipes.find_one_and_update({'count': {'$exists': True}}, {'$inc': {'ExceptCount:': 1}})
            print(ae)
# load files

# get page

# check if page already scrapped

# scrap page

# controle recipe

# save as valid or junk in var

# save in file
