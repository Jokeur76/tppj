let getRequests = {
    ing: ing,
    unit: unit,
    cat: cat,
    totalCourses: totalCourses,
    isAdmin: isAdmin
}

let app = require(global.__base+'app.js');
let dbName = JSON.parse(fs.readFileSync(global.__base+'conf/env.json', 'utf8')).dbName;

/** collections mongos */
const COL_USER = "user";
const COL_COURSES = "courses";
const COL_INGREDIENTS = "ingredients";
const COL_UNITS = "units";
const COL_CATS = "cats";

let url = 'mongodb://localhost:27017/'+dbName;

function ing(req, res, next){
    myMongo.getCollection(COL_INGREDIENTS)
        .then(function(datas) {
            res.send(datas);
        })
        .catch(next);
}

function unit(req, res, next){
    myMongo.getCollection(COL_UNITS)
        .then(function(datas) {
            res.send(datas);
        })
        .catch(next);
}

function cat(req, res, next){
    myMongo.getCollection(COL_CATS)
        .then(function(datas) {
            res.send(datas);
        })
          .catch(next);
}

function totalCourses(req, res, next){
    myMongo.countCollection(COL_COURSES, function(err, count){
        res.send({count: count});
    })
      .catch(next);
}

function isAdmin(req, res, next){
    myMongo.isUserAdmin(app.getSess().userId, function(isAdmin) {
        res.send(isAdmin);
    })
      .catch(next);
}

module.exports = getRequests;