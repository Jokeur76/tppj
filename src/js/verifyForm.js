function checkCourseForm() {
    var check = 0;
    $('.diet_type').each(function(){
        if($(this).is(':checked')){
            check++;
        }
    });

    var testNotNull = true;
    $('.notNull').each(function(){
        if($(this).val() == "" || $(this).val() == null || $(this).val() == NaN) {
            testNotNull = false;
        }
    });
    if(testNotNull){
        check++;
    }

    if(check == 2) {
        $('#courseForm').submit();
    } else {
        console.log('[ERROR] target : check == 2, result : check == '+check);
    }
    if(currentStep) currentStep = "ingName";

}

function checkMenuForm() {
    var check = 0;
    $('.diet_type').each(function(){
        if($(this).is(':checked')){
            check++;
        }
    });

    if(check == 1) {
        $('#menuForm').submit();
    } else {
        console.log('[ERROR] target : check == 1, result : check == '+check);
    }
}

function checkPwdResetForm() {
    var check = 0;
    if($('#pwd').val() === $('#pwd_confirm').val()) {
        check++;
    } else {
        toyoster(pwdDontMatch);
    }

    if(check == 1) {
        $('#pwdresetForm').submit();
    } else {
        console.log('[ERROR] target : check == 1, result : check == '+check);
    }
}