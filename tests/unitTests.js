global.__base = __dirname.replace('tests', '');
global.__env = "unit-tests";

let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let should = chai.should();

let myMongo = require('../modules/myMongo.js');
let app = require(global.__base+'app.js');
let postRequests = require(global.__base+'modules/postRequests.js');

let url = 'mongodb://localhost:27017/tppj-test';
let testCpt = 0;
let cptTarget = 4;
let test_collection = "test";
let testUser = {
    "signUp" : 1467122568866,
    "login" : "usertest",
    "pwd" : "$2a$10$RJmt2Rf9CRUPj0yJPgy3seRcvQx57FjoUh.2fdvDEwXyrfeJw7lQW",
    "pseudo" : "test",
    "diet" : "test",
    "isAdmin" : true
};
let notAdminTestUser = {
    "signUp" : 1467122568866,
    "login" : "notAdminTestUser",
    "pwd" : "$2a$10$RJmt2Rf9CRUPj0yJPgy3seRcvQx57FjoUh.2fdvDEwXyrfeJw7lQW",
    "pseudo" : "notAdminTestUser",
    "diet" : "notAdmin",
    "isAdmin" : false
};

let ingSize = 531;
let catSize = 15;
let unitSize = 20;

myMongo.setUrl(url);

//myMongo.dropTestDB();
beforeEach(function() {
    return myMongo.createStartList('test').then(function(){
        //nothing to do
        //console.log("list created");
    }, function(err) {
        console.log("list NOT created : "+err);
    })
});


afterEach(function() {
    return myMongo.dropTestDB().then(function(data) {
        //nothing to do
        //console.log("DB Dropped");
    }, function(err) {
        console.log(err);
    })
});

//assertions test
describe('assertions', function() {
    it('passe', function() {
        let test = "test";
        should.not.equal(null, test);
        expect(test).to.be.a('string');
        assert.equal(test, "test");
        testCpt++;
    });
});

describe('asynchronous', function() {
    it('is asynchronous success', function(done) {
        setTimeout(function() {
            expect(true).to.equal(true);
            done()
        }, 4)

    });
});

//tools modules
describe('createId', function() {
    it('must return a usable id', function() {
        let serverTools = require('../modules/serverTools.js');
        let testString = "tèst réussï";

        let result = serverTools.createId(testString);
        assert.equal(result, "test_reussi");
    });
});

describe('count', function() {
    it('must return 1', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(test_collection).insertOne({test: "succeed"}, function(err, results) {
                assert.equal(err, null, "err : "+err);
                myMongo.countCollection(test_collection, function(err, result) {
                    assert.equal(result, 1);
                    done();
                })
            })
        })
    });
});

describe('createStartList', function() {
    //method run by beforeEach
    it('must create ' + catSize + ' cat', function(done) {
        myMongo.countCollection(COL_CATS, function(err, result) {
            assert.equal(result, catSize);
            done();
        });
    });
    it('must create ' + ingSize + ' ingredients', function(done) {
        myMongo.countCollection(COL_INGREDIENTS, function(err, result) {
            assert.equal(result, ingSize);
            done();
        });
    });
    it('must create ' + unitSize + ' units', function(done) {
        myMongo.countCollection(COL_UNITS, function(err, result) {
            assert.equal(result, unitSize);
            done();
        });
    });
});

describe('createUser', function() {
    it('must add an entry into user collection', function(done) {
        myMongo.createUser(testUser.cloneIt(), function(result) {
            assert.equal(result.ops.length, 1);
            assert.equal(result.ops[0].signUp, testUser.signUp);
            assert.equal(result.ops[0].login, testUser.login);
            assert.equal(result.ops[0].pwd, testUser.pwd);
            assert.equal(result.ops[0].pseudo, testUser.pseudo);
            assert.equal(result.ops[0].diet, testUser.diet);
            assert.equal(result.ops[0].isAdmin, testUser.isAdmin);
            //test two creations for the need of others tests
            myMongo.createUser(notAdminTestUser.cloneIt(), function(result) {
                assert.equal(result.ops.length, 1);
                assert.equal(result.ops[0].signUp, notAdminTestUser.signUp);
                assert.equal(result.ops[0].login, notAdminTestUser.login);
                assert.equal(result.ops[0].pwd, notAdminTestUser.pwd);
                assert.equal(result.ops[0].pseudo, notAdminTestUser.pseudo);
                assert.equal(result.ops[0].diet, notAdminTestUser.diet);
                assert.equal(result.ops[0].isAdmin, notAdminTestUser.isAdmin);
                done();
            })
        })
    });
});

describe('getBuyUnitFromName', function() {
    it('must return Gousse(s)', function(done) {
        myMongo.getBuyUnitFromName("Ail", function(buyUnit) {
            assert.equal("Gousse(s)", buyUnit);
            done();
        })
    });
    it('must return null', function(done) {
        myMongo.getBuyUnitFromName("existepas", function(buyUnit) {
            assert.isNull(buyUnit);
            done();
        })
    });
});

describe('getCourseByTitle', function() {
    let course = {
            "name" : "entrée_7",
            "creation_date" : 1503589691471,
            "type" : 2,
            "diet" : 1,
            "portion_nbr" : 5,
            "ingList" : [
                {
                    "name" : "ail",
                    "qtt" : 2,
                    "unit" : "Gousse(s)",
                    "buyUnit" : "Gousse(s)"
                },
                {
                    "name" : "carotte(s)",
                    "qtt" : 1,
                    "unit" : "Kilo(s)",
                    "buyUnit" : "Kilo(s)"
                },
                {
                    "name" : "carotte(s)",
                    "qtt" : 200,
                    "unit" : "Gramme(s)",
                    "buyUnit" : "Kilo(s)"
                }
            ],
            "alpha" : true,
            "pv" : false,
            "userId" : "autoGenerated",
            "recLink" : "link.fr"
        }
    ;
    it('must return a course', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_COURSES).insertOne(course.cloneIt(), function(results) {
                assert.equal(err, null, "err : "+err);
                myMongo.getCourseByTitle(course.name, function(savedCourse) {
                    should.exist(savedCourse);
                    assert.equal(savedCourse.name, "entrée_7");
                    assert.equal(savedCourse.creation_date, 1503589691471);
                    assert.equal(savedCourse.type, 2);
                    assert.equal(savedCourse.diet, 1);
                    assert.equal(savedCourse.portion_nbr, 5);
                    assert.equal(savedCourse.ingList[0].name, "ail");
                    assert.equal(savedCourse.ingList[0].qtt, 2);
                    assert.equal(savedCourse.ingList[0].unit, "Gousse(s)");
                    assert.equal(savedCourse.ingList[0].buyUnit, "Gousse(s)");
                    assert.equal(savedCourse.ingList[1].name, "carotte(s)");
                    assert.equal(savedCourse.ingList[1].qtt, 1);
                    assert.equal(savedCourse.ingList[1].unit, "Kilo(s)");
                    assert.equal(savedCourse.ingList[1].buyUnit, "Kilo(s)");
                    assert.equal(savedCourse.ingList[2].name, "carotte(s)");
                    assert.equal(savedCourse.ingList[2].qtt, 200);
                    assert.equal(savedCourse.ingList[2].unit, "Gramme(s)");
                    assert.equal(savedCourse.ingList[2].buyUnit, "Kilo(s)");
                    assert.equal(savedCourse.alpha, true);
                    assert.equal(savedCourse.pv, false);
                    assert.equal(savedCourse.userId, "autoGenerated");
                    assert.equal(savedCourse.recLink, "link.fr");
                    done();
                })
            })
        })
    });
    it('must return null', function(done) {
        myMongo.getCourseByTitle("nomdeplatjamaisutiliseenfinjesperesinoncavafaireplanterletestpourrien", function(savedCourse) {
            assert.equal(savedCourse, null);
            done();
        })
    });
});

describe('getUserByLogin', function() {
    it('must return the complete user', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_USER).insertOne(testUser.cloneIt(), function(err, result) {
                assert.equal(err, null, "err : "+err);
                myMongo.getUserByLogin(testUser.login, function(returnedUser) {
                    should.exist(returnedUser);
                    assert.equal(returnedUser.signUp, testUser.signUp);
                    assert.equal(returnedUser.login, testUser.login);
                    assert.equal(returnedUser.pwd, testUser.pwd);
                    assert.equal(returnedUser.pseudo, testUser.pseudo);
                    assert.equal(returnedUser.diet, testUser.diet);
                    assert.equal(returnedUser.isAdmin, testUser.isAdmin);
                    done();
                })
            });
        });
    });
    it('must return null when login not exist', function(done) {
        myMongo.getUserByLogin("pseudojamaisutiliseenfinjesperesinoncavafaireplanterletestpourrien", function(returnedUser) {
            assert.equal(null, returnedUser);
            done();
        })

    });
});

describe('insertDocument', function() {
    it('must create a document', function(done) {
        let doc = {
            test: 'reussi'
        };
        myMongo.insertDocument(test_collection, doc, function(result) {
            assert.equal(result.ops.length, 1);
            done()
        })
    });
});

describe('isHashValid', function() {
    it("must return false because hash don't exist", function(done) {
        myMongo.isHashValid("hashjamaisutiliseenfinjesperesinoncavafaireplanterletestpourrien", function(isValid) {
            assert.equal(isValid, false);
            done()
        })
    });
    let doc = {
        "hash" : "$2a$05$zSqAi8TRbxMyBMeQO8skde2RuY3J0jAgcgEzQktVvTgwqFMhyVZH6",
        "emailToReset" : "mickael.larcheveque@gmail.com",
        "moment" : new Date().getTime()
    };
    it('must return true', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_RESET).insertOne(doc.cloneIt(), function(err, result) {
                assert.equal(err, null, "err : "+err);
                myMongo.isHashValid(doc.hash, function(isValid) {
                    assert.equal(isValid, true);
                    done()
                })
            })
        })
    });
    it('must return false because hash to old', function(done) {
        doc.hash = "$2a$05$.7kIV9Ujr3dyR9OqCN/Wnur15Z0oVcLz9wIsFI.NETWEixa86d2H.";
        doc.moment = new Date().getTime() - 25*60*60*1000; //set moment of creeation of hash 25h before now
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_RESET).insertOne(doc.cloneIt(), function(err, result) {
                assert.equal(err, null, "err : "+err);
                myMongo.isHashValid(doc.hash, function(isValid) {
                    assert.equal(isValid, false);
                    done()
                })
            })
        })
    });
});

describe('isLoginAvailable', function() {
    let isLoginAvailableTestUser = {
        "signUp" : 1467122568866,
        "login" : "isLoginAvailableTestUser",
        "pwd" : "$2a$10$RJmt2Rf9CRUPj0yJPgy3seRcvQx57FjoUh.2fdvDEwXyrfeJw7lQW",
        "pseudo" : "test",
        "diet" : "test",
        "isAdmin" : true
    };
    mongoclient.connect(url, function(err, db){
        assert.equal(err, null, "err : "+err);
        db.collection(COL_USER).insertOne(isLoginAvailableTestUser.cloneIt(), function(err, result) {
            assert.equal(err, null, "err : "+err);
            it('must return true', function(done) {
                myMongo.isLoginAvailable(isLoginAvailableTestUser.login, function(isAvailable) {
                    assert.equal(isAvailable, false);
                    done();
                })
            });
            it('must return false', function(done) {
                myMongo.isLoginAvailable("pseudojamaisutiliseenfinjesperesinoncavafaireplanterletestpourrien", function(isAvailable) {
                    assert.equal(isAvailable, true);
                    done();
                })
            });
        });
    });
});

describe('isUserAdmin', function() {
    it('must say that user is admin', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_USER).insertOne(testUser.cloneIt(), function(err, result) {
                assert.equal(err, null, "err : "+err);
                myMongo.isUserAdmin(result.ops[0]._id, function(isAdmin) {
                    assert.equal(isAdmin, true);
                    done();
                })
            });
        });
    });
    it('must say that user is not  admin', function(done) {
        mongoclient.connect(url, function(err, db){
            assert.equal(err, null, "err : "+err);
            db.collection(COL_USER).insertOne(notAdminTestUser.cloneIt(), function(err, result) {
                assert.equal(err, null, "err : "+err);
                myMongo.isUserAdmin(result.ops[0]._id, function(isAdmin) {
                    assert.equal(isAdmin, false);
                    done();
                })
            });
        });

    });
});

//Post Requests tests
let testEntry = {
    mainCourses: [
        {
            _id: '599ef53bcf9431e5457cd0ff',
            name: 'plat_21',
            creation_date: 1503589691467,
            type: 4,
            diet: 1,
            portion_nbr: 6,
            ingList:
                [
                    {
                        name: 'carotte(s)',
                        qtt: 1,
                        unit: 'Kilo(s)',
                        buyUnit: 'Kilo(s)'
                    },
                    {
                        name: 'carotte(s)',
                        qtt: 200,
                        unit: 'Gramme(s)',
                        buyUnit: 'Kilo(s)'
                    },
                    {
                        name: 'chocolat',
                        qtt: 500,
                        unit: 'Gramme(s)',
                        buyUnit: 'Gramme(s)'
                    }
                ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd0f2',
            name: 'plat_8',
            creation_date: 1503589691461,
            type: 4,
            diet: 1,
            portion_nbr: 5,
            ingList:
                [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                    { name: 'carotte(s)',
                        qtt: 200,
                        unit: 'Gramme(s)',
                        buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 0.5,
                        unit: 'Kilo(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd0f9',
            name: 'plat_15',
            creation_date: 1503589691464,
            type: 4,
            diet: 1,
            portion_nbr: 4,
            ingList:
                [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                    { name: 'carotte(s)',
                        qtt: 200,
                        unit: 'Gramme(s)',
                        buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 500,
                        unit: 'Gramme(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd104',
            name: 'plat_26',
            creation_date: 1503589691469,
            type: 4,
            diet: 1,
            portion_nbr: 3,
            ingList:
                [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                    { name: 'carotte(s)',
                        qtt: 1,
                        unit: 'Kilo(s)',
                        buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 500,
                        unit: 'Gramme(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd0ec',
            name: 'plat_2',
            creation_date: 1503589691459,
            type: 4,
            diet: 1,
            portion_nbr: 3,
            ingList:
                [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                    { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 0.5,
                        unit: 'Kilo(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd0f8',
            name: 'plat_14',
            creation_date: 1503589691464,
            type: 4,
            diet: 1,
            portion_nbr: 5,
            ingList:
                [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                    { name: 'carotte(s)',
                        qtt: 1,
                        unit: 'Kilo(s)',
                        buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 500,
                        unit: 'Gramme(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }, {
            _id: '599ef53bcf9431e5457cd103',
            name: 'plat_25',
            creation_date: 1503589691468,
            type: 4,
            diet: 1,
            portion_nbr: 2,
            ingList:
                [ { name: 'carotte(s)',
                    qtt: 3,
                    unit: 'Unité(s)',
                    buyUnit: 'Kilo(s)' },
                    { name: 'chocolat',
                        qtt: 500,
                        unit: 'Gramme(s)',
                        buyUnit: 'Gramme(s)' },
                    { name: 'chocolat',
                        qtt: 0.5,
                        unit: 'Kilo(s)',
                        buyUnit: 'Gramme(s)' } ],
            alpha: true,
            pv: false,
            userId: 'autoGenerated',
            recLink: ''
        }
    ],
    starters:
        [
            {
                _id: '599ef53bcf9431e5457cd11c',
                name: 'entrée_22',
                creation_date: 1503589691474,
                type: 2,
                diet: 1,
                portion_nbr: 3,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd11d',
                name: 'entrée_23',
                creation_date: 1503589691474,
                type: 2,
                diet: 1,
                portion_nbr: 5,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd10e',
                name: 'entrée_8',
                creation_date: 1503589691471,
                type: 2,
                diet: 1,
                portion_nbr: 4,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 3,
                            unit: 'Unité(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd10d',
                name: 'entrée_7',
                creation_date: 1503589691471,
                type: 2,
                diet: 1,
                portion_nbr: 5,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 1,
                            unit: 'Kilo(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd113',
                name: 'entrée_13',
                creation_date: 1503589691471,
                type: 2,
                diet: 1,
                portion_nbr: 2,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 3,
                            unit: 'Unité(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd110',
                name: 'entrée_10',
                creation_date: 1503589691471,
                type: 2,
                diet: 1,
                portion_nbr: 2,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 1,
                            unit: 'Kilo(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd10c',
                name: 'entrée_6',
                creation_date: 1503589691471,
                type: 2,
                diet: 1,
                portion_nbr: 4,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' },
                        { name: 'chocolat',
                            qtt: 0.5,
                            unit: 'Kilo(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd11e',
                name: 'entrée_24',
                creation_date: 1503589691475,
                type: 2,
                diet: 1,
                portion_nbr: 3,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 3,
                            unit: 'Unité(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }
        ],
    desserts:
        [
            {
                _id: '599ef53bcf9431e5457cd129',
                name: 'dessert_7',
                creation_date: 1503589691477,
                type: 1,
                diet: 1,
                portion_nbr: 3,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 1,
                            unit: 'Kilo(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd13c',
                name: 'dessert_26',
                creation_date: 1503589691479,
                type: 1,
                diet: 1,
                portion_nbr: 5,
                ingList:
                    [ { name: 'ail', qtt: 2, unit: 'Gousse(s)', buyUnit: 'Gousse(s)' },
                        { name: 'carotte(s)',
                            qtt: 3,
                            unit: 'Unité(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd138',
                name: 'dessert_22',
                creation_date: 1503589691479,
                type: 1,
                diet: 1,
                portion_nbr: 4,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' },
                        { name: 'chocolat',
                            qtt: 0.5,
                            unit: 'Kilo(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd13d',
                name: 'dessert_27',
                creation_date: 1503589691479,
                type: 1,
                diet: 1,
                portion_nbr: 4,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 1,
                        unit: 'Kilo(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd139',
                name: 'dessert_23',
                creation_date: 1503589691479,
                type: 1,
                diet: 1,
                portion_nbr: 2,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 1,
                        unit: 'Kilo(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' },
                        { name: 'chocolat',
                            qtt: 0.5,
                            unit: 'Kilo(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd132',
                name: 'dessert_16',
                creation_date: 1503589691478,
                type: 1,
                diet: 1,
                portion_nbr: 4,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 1,
                            unit: 'Kilo(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd124',
                name: 'dessert_2',
                creation_date: 1503589691477,
                type: 1,
                diet: 1,
                portion_nbr: 3,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 200,
                            unit: 'Gramme(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 0.5,
                            unit: 'Kilo(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }, {
                _id: '599ef53bcf9431e5457cd13a',
                name: 'dessert_24',
                creation_date: 1503589691479,
                type: 1,
                diet: 1,
                portion_nbr: 3,
                ingList:
                    [ { name: 'carotte(s)',
                        qtt: 3,
                        unit: 'Unité(s)',
                        buyUnit: 'Kilo(s)' },
                        { name: 'carotte(s)',
                            qtt: 1,
                            unit: 'Kilo(s)',
                            buyUnit: 'Kilo(s)' },
                        { name: 'chocolat',
                            qtt: 500,
                            unit: 'Gramme(s)',
                            buyUnit: 'Gramme(s)' } ],
                alpha: true,
                pv: false,
                userId: 'autoGenerated',
                recLink: ''
            }
        ],
    smallestPortionNbrSum: 28,
    requestedTypes: 7
},
    startAt = '1504389600000',
    people_nbr = 2;

describe("createMenu", function() {
    it('must return a menu with smallestPortionNbrSum cumulated portions for each type of course and the same nbr of portion per course', function() {
        let menu = postRequests.createMenu(testEntry.cloneIt(), startAt, people_nbr);
        let cumulForTest = {
            starters: {cumulPortions: 0},
            mainCourses: {cumulPortions: 0},
            desserts: {cumulPortions: 0}
        }
        for(let services of menu.obj) {
            for(let service in services) {
                for(let courseType in services[service]) {
                    for(let course of services[service][courseType]) {
                        if(cumulForTest[courseType][course.name] === undefined) {
                            cumulForTest[courseType][course.name] = course;
                        } else {
                            cumulForTest[courseType][course.name].portion_nbr += course.portion_nbr
                        }
                        cumulForTest[courseType].cumulPortions += course.portion_nbr
                    }
                }
            }
        }
        for(let courseType in cumulForTest) {
            expect(cumulForTest[courseType].cumulPortions).to.be.equal(testEntry.smallestPortionNbrSum)
            for(let courseName in cumulForTest[courseType]) {
                if(courseName !== "cumulPortions") {
                    let cumulatedPortionNbr = cumulForTest[courseType][courseName].portion_nbr;
                    let expectedportionNbr;
                    for(let course of testEntry[courseType]) {
                        if(course.name === courseName) {
                            expectedportionNbr = course.portion_nbr;
                            break;
                        }
                    }
                    expect(cumulatedPortionNbr).to.be.equal(expectedportionNbr)
                }
            }
        }
    })
});

describe("createIngList", function() {
    let ingredients = {}, units = {};
    let ingId, unitId;
    let testOutput, expectedOutput;
    it('must return a ingredient list with the same cumulated quantity of ingredients than the testEntry', function() {
        for(let courseType in testEntry) {
            if(courseType === "starters" || courseType === "mainCourses" || courseType === "desserts") {
                for(let course of testEntry[courseType]) {
                for(let ing of course.ingList) {
                    //convert unit to buy unit
                    ing = postRequests.normalizeIng(ing.cloneIt());
                    //get usable html id for ing and unit from names
                    ingId = serverTools.createId(ing.name);
                    unitId = serverTools.createId(ing.unit);
                    //if ingredient don't exist yet
                    if(ingredients[ingId] === undefined) {
                        //create unit objects
                        units = {};
                        units[unitId] = {
                            name: ing.unit,
                            qtt: ing.qtt
                        };
                        //add it
                        ingredients[ingId] = {
                            name: ing.name,
                            units: units
                        }
                    } else {
                        // incremente quantity for existing ing
                        if(ingredients[ingId].units[unitId] === undefined) {
                            //if unit don't exist for this ingredient : add it
                            ingredients[ingId].units[unitId] = {
                                name: ing.unit,
                                qtt: ing.qtt
                            };
                        } else {
                            //if unit already exist for this ingredient : increment it
                            ingredients[ingId].units[unitId].qtt += ing.qtt;
                        }
                    }
                }
            }
            }
        }

        expectedOutput = postRequests.createIngHtmlList(ingredients);
        testOutput = postRequests.createIngList(testEntry.cloneIt());
        expect(testOutput).to.be.equal(expectedOutput);
    })
});

describe("normalizeIng", function() {
    let ingredient = {
        name: 'carotte(s)',
        qtt: 200,
        unit: 'Gramme(s)',
        buyUnit: 'Kilo(s)'
    };
    it('must return a ingredient with 0.2kg as qtt/unit', function() {
        let ing = postRequests.normalizeIng(ingredient);
        expect(ing.qtt).to.be.equal(0.2);
        expect(ing.unit).to.be.equal(ingredient.buyUnit);
    })
});

describe("getCoursesFromDb", function() {
    let onlyMine = false,
        diet_type = 1,
        course_type = "4",
        people_nbr = 2,
        totalPortionNbr = 28;
    let generateCourses = require(global.__base+"resources/scripts/generateCourses.js");

    it('must return an array of totalPortionNbr courses of diet_type and course_type with people_nbr portion min and people_nbr*3 portion max', function() {
        return generateCourses.createBasicsCourses()
            .then(() => {
                return postRequests.getCoursesFromDb(onlyMine, diet_type, parseInt(course_type), people_nbr, totalPortionNbr)
                .then(function(results) {
                    expect(results.length).to.be.equal(totalPortionNbr);
                    for(let course of results) {
                        expect(course.portion_nbr).to.be.at.least(people_nbr);
                        expect(course.portion_nbr).to.be.at.most(people_nbr*3)
                        expect(course.diet).to.be.equal(diet_type);
                        expect(course.type).to.be.equal(parseInt(course_type));
                    }
                })
            })
    })
});
