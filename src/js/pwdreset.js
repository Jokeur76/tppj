$(function () {
    if(window.location.pathname === "/pwdreset") {
        $('#resetBtn').click(function(event) {
            event.preventDefault();
            checkPwdResetForm();
        });
        $('#hash').val(getUrlParam("hash"));
    }
});