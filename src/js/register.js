$(function(){
    if(window.location.pathname === "/register") {
        $('#register_btn').click(function(event) {
            checkRegisterForm();
        });
        displayToyost();
    }});

function checkRegisterForm(){
    $('#registerForm').submit();
}

function showPwd() {
    var pwd = $('#register_pwd');
    if(pwd.attr('type') == "password") {
        pwd.attr('type', 'text');
    } else {
        pwd.attr('type', 'password');
    }
}